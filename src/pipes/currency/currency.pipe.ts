import { Pipe, PipeTransform } from '@angular/core';
var formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD'
  });

@Pipe({
  name: 'currency'
})
export class CurrencyPipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    return formatter.format(value);
  }

}
