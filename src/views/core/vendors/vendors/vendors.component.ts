/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of vendors.
 *
 * @export
 * @class VendorsComponent
 */
@Component({
	selector: 'app-vendors',
	templateUrl: './vendors.component.html',
	styleUrls: [ './vendors.component.scss' ]
})
export class VendorsComponent {
	/**
	 * Local reference to array of vendors.
	 *
	 * @type {Array<object>}
	 * @memberof VendorsComponent
	 */
	public vendors: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public originalVendors: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of VendorsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof VendorsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof VendorsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.vendors = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of vendors.
	 *
	 * @returns {Promise<any>}
	 * @memberof VendorsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const vendors = await this.apiProvider.doGet('Vendors/GetVendors');
			this.vendors = vendors['data'];
			this.originalVendors = vendors['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof VendorsComponent
	 */
	public search(text): void {
		if (text === "") {
			this.vendors = this.originalVendors;
		} else if (this.originalVendors.length > 0) {
			this.vendors = this.originalVendors.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof VendorsComponent
	 */
	public addVendor(vendor = null): void {
		this.router.navigate([ { outlets: { app: 'vendor' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: vendor
			}
		});
	}
}
