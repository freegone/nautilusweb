import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of vendor.
 *
 * @export
 * @class VendorComponent
 */
@Component({
	selector: 'app-vendor',
	templateUrl: './vendor.component.html',
	styleUrls: [ './vendor.component.scss' ]
})
export class VendorComponent implements OnInit {
	/**
	 * Save vendor array.
	 *
	 * @type {Array<object>}
	 * @memberof VendorComponent
	 */
	public companies: Array<object>;
	/**
	 * Save vendor object.
	 *
	 * @type {Array<object>}
	 * @memberof VendorComponent
	 */
	public vendor: any;
	/**
 	 *. Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof VendorComponent
	 */
	public inputs: Array<object> = inputs;
	/**
 	 *. Selects template.
	 *
	 * @type {Array<object>}
	 * @memberof VendorComponent
	 */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof VendorComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of VendorComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof VendorComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.vendor = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.vendor ? this.vendor.id : 0),
			companyId: new FormControl(this.vendor ? this.vendor.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.vendor ? this.vendor.officeId : '', [ Validators.required ]),
			name: new FormControl(this.vendor ? this.vendor.name : '', [
				Validators.required,
				Validators.maxLength(250)
			]),
			rfc: new FormControl(this.vendor ? this.vendor.rfc : '', [ Validators.maxLength(50) ]),
			email: new FormControl(this.vendor ? this.vendor.email : '', [ Validators.email, Validators.maxLength(150) ]),
			phone: new FormControl(this.vendor ? this.vendor.phone : '', [ Validators.maxLength(10) ]),
			address: new FormControl(this.vendor ? this.vendor.address : '', [ Validators.maxLength(250) ]),
			notes: new FormControl(this.vendor ? this.vendor.notes : '', [ Validators.maxLength(300) ])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof VendorComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.vendor ? [this.vendor.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.vendor ? [this.vendor.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add vendor in database.
	 *
	 * @memberof VendorComponent
	 */
	public async saveVendor(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Vendors/UpdateVendor', form);
			} else {
				response = await this.apiProvider.doPost('Vendors/SaveVendor', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof VendorComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.vendor) {
			form['id'] = this.vendor.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete vendor from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof VendorComponent
	 */
	public async deleteVendor(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el proveedor ' + this.vendor.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Vendors/DeleteVendor', this.vendor.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
	 * Enter here when clicked cancel button.
	 *
	 * @memberof VendorComponent
	 */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'vendors' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
