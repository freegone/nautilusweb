/*Angular Imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of companies.
 *
 * @export
 * @class CompaniesComponent
 */
@Component({
	selector: 'app-companies',
	templateUrl: './companies.component.html',
	styleUrls: [ './companies.component.scss' ]
})
export class CompaniesComponent {
	/**
	 * Local reference to array of templates.
	 *
	 * @type {Array<object>}
	 * @memberof CompaniesComponent
	 */
	public companies: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * @type {Array<object>}
	 * @memberof CompaniesComponent
	 */
	public originalCompanies: Array<object> = [];
	/**
	 * Seach input string.
	 *
	 * @type {object}
	 * @memberof CompaniesComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof CompaniesComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of CompaniesComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof CompaniesComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof CompaniesComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.companies = [];
		this.loadTemplates();
	}
	/**
		 * The function to load templates from database of companies.
		 *
		 * @returns {Promise<any>}
		 * @memberof CompaniesComponent
		 */
	public async loadTemplates(): Promise<any> {
		try {
			const resp = await this.apiProvider.doGet('Companies/GetCompanies');
			this.companies = resp['data'];
			this.originalCompanies = resp['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof UsersComponent
	 */
	 public search(text): void {
		if (text === "") {
			this.companies = this.originalCompanies;
		} else if (this.originalCompanies.length > 0) {
			this.companies = this.originalCompanies.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button
	 *
	 * @param {null} user
	 * @memberof CompaniesComponent
	 */
	public addCompany(company: null): void {
		this.router.navigate([ { outlets: { app: 'company' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: company
			}
		});
	}
}
