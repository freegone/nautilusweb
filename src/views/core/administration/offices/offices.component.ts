/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of offices.
 *
 * @export
 * @class OfficesComponent
 */
@Component({
	selector: 'app-offices',
	templateUrl: './offices.component.html',
	styleUrls: [ './offices.component.scss' ]
})
export class OfficesComponent {
	/**
	 * Local reference to array of offices.
	 *
	 * @type {Array<object>}
	 * @memberof OfficesComponent
	 */
	public offices: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * @type {Array<object>}
	 * @memberof OfficesComponent
	 */
	public originalOffices: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof OfficesComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof OfficesComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of OfficesComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof OfficesComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof OfficesComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.offices = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof OfficesComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.offices = offices['data'];
			this.originalOffices = offices['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof OfficesComponent
	 */
	public search(text): void {
		if (text === "") {
			this.offices = this.originalOffices;
		} else if (this.originalOffices.length > 0) {
			this.offices = this.originalOffices.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof OfficesComponent
	 */
	public addOffice(office = null): void {
		this.router.navigate([ { outlets: { app: 'office' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: office
			}
		});
	}
}
