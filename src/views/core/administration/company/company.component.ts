/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs } from './template.json';
/**
 * The component of company.
 *
 * @export
 * @class CompanyComponent
 */
@Component({
	selector: 'app-company',
	templateUrl: './company.component.html',
	styleUrls: [ './company.component.scss' ]
})
export class CompanyComponent {
	/**
	 * Save company object.
	 *
	 * @type {Array<object>}
	 * @memberof CompanyComponent
	 */
	public company: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof CompanyComponent
   */
	public inputs: Array<object> = inputs;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof CompanyComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of CompanyComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof CompanyComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.company = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.company ? this.company.id : 0),
			name: new FormControl(this.company ? this.company.name : '', [
				Validators.required,
				Validators.maxLength(250)
			]),
			rfc: new FormControl(this.company ? this.company.rfc : '', [ Validators.maxLength(50) ]),
			email: new FormControl(this.company ? this.company.email : '', [ Validators.email, Validators.maxLength(150) ]),
			phone: new FormControl(this.company ? this.company.phone : '', [ Validators.maxLength(10) ]),
			address: new FormControl(this.company ? this.company.address : '', [ Validators.maxLength(250) ]),
			notes: new FormControl(this.company ? this.company.notes : '', [ Validators.maxLength(300) ]),
			picture: new FormControl(this.company ? this.company.picture : '')
		});
	}
	/**
   * Enter here when upload file in input component.
   *
   * @param {*} file
   * @memberof CompanyComponent
   */
	public setImage(file): void {
		if (file) {
			if (file === 'default') {
				document.getElementById('img-usr')['src'] = '../../../assets/img/company_default.svg';
			} else {
				var reader = new FileReader();
				reader.onload = (e: any) => {
					document.getElementById('img-usr')['src'] = e.target.result;
				};
				reader.readAsDataURL(file);
			}
		}
	}
	/**
	 * Add company in database.
	 *
	 * @returns {Promise<any>}
	 * @memberof CompanyComponent
	 */
	public async addCompany(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			if (form['id']) {
				await this.apiProvider.doPut('Companies/UpdateCompany', form);
			} else {
				await this.apiProvider.doPost('Companies/SaveCompany', form);
			}
			this.doCancel();
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Return values.
	 *
	 * @private
	 * @returns {object}
	 * @memberof CompanyComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.company) {
			form['id'] = this.company.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		return form;
	}
	/**
	 * Delete company from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof CompanyComponent
	 */
	public async deleteCompany(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar a la compañia ' + this.company.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					const response = await this.apiProvider.doDelete('Companies/DeleteCompany', this.company.id);
					if (response['data'] === null) {
						Swal.fire({
							type: 'error',
							title: 'Lo sentimos',
							confirmButtonColor: '#313731ad',
							text:
								'Esta compañía tiene sucursales asociadas, remueve esta asociación para completar el proceso.'
						});
					} else {
						this.doCancel();
					}
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
   * Enter here when clicked cancel button.
   *
   * @memberof CompanyComponent
   */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'companies' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
