import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of office.
 *
 * @export
 * @class OfficeComponent
 */
@Component({
	selector: 'app-office',
	templateUrl: './office.component.html',
	styleUrls: [ './office.component.scss' ]
})
export class OfficeComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof OfficeComponent
	 */
	public companies: Array<object>;
	/**
	 * Save office object.
	 *
	 * @type {Array<object>}
	 * @memberof OfficeComponent
	 */
	public office: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof OfficeComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof OfficeComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof OfficeComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of OfficeComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof OfficeComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.office = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.office ? this.office.id : 0),
			companyId: new FormControl(this.office ? this.office.companyId : '', [ Validators.required ]),
			name: new FormControl(this.office ? this.office.name : '', [ Validators.required, Validators.maxLength(250) ]),
			address: new FormControl(this.office ? this.office.address : '', [ Validators.maxLength(250) ]),
			notes: new FormControl(this.office ? this.office.notes : '', [ Validators.maxLength(350) ]),
			picture: new FormControl(this.office ? this.office.picture : '')
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof OfficeComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.office ? [this.office.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter here when upload file in input component.
	 *
	 * @param {*} file
	 * @memberof CompanyComponent
	 */
	 public setImage(file): void {
		if (file) {
			if (file === 'default') {
				document.getElementById('img-usr')['src'] = '../../../assets/img/company_default.svg';
			} else {
				var reader = new FileReader();
				reader.onload = (e: any) => {
					document.getElementById('img-usr')['src'] = e.target.result;
				};
				reader.readAsDataURL(file);
			}
		}
	}
	/**
	 * Add office in database.
	 *
	 * @memberof OfficeComponent
	 */
	public async saveOffice(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Offices/UpdateOffice', form);
			} else {
				response = await this.apiProvider.doPost('Offices/SaveOffice', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof OfficeComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.office) {
			form['id'] = this.office.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete office from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof OfficeComponent
	 */
	public async deleteOffice(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar la sucursal ' + this.office.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					const response = await this.apiProvider.doDelete('Offices/DeleteOffice', this.office.id);
					if (response['data'] === null) {
						Swal.fire({
							type: 'error',
							title: 'Lo sentimos',
							confirmButtonColor: '#313731ad',
							text:
								'Esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
						});
					} else {
						this.doCancel();
					}
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof OfficeComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'offices' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
