/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of customers.
 *
 * @export
 * @class CustomersComponent
 */
@Component({
	selector: 'app-customers',
	templateUrl: './customers.component.html',
	styleUrls: [ './customers.component.scss' ]
})
export class CustomersComponent {
	/**
	 * Local reference to array of customers.
	 *
	 * @type {Array<object>}
	 * @memberof CustomersComponent
	 */
	public customers: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof CustomersComponent
	 */
	public originalCustomers: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof CustomersComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof CustomersComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of CustomersComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof CustomersComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof CustomersComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.customers = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of customers.
	 *
	 * @returns {Promise<any>}
	 * @memberof CustomersComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const customers = await this.apiProvider.doGet('Customers/GetCustomers');
			this.customers = customers['data'];
			this.originalCustomers = customers['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof CustomersComponent
	 */
	public search(text): void {
		if (text === "") {
			this.customers = this.originalCustomers;
		} else if (this.originalCustomers.length > 0) {
			this.customers = this.originalCustomers.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof CustomersComponent
	 */
	public addCustomer(customer = null): void {
		this.router.navigate([ { outlets: { app: 'customer' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: customer
			}
		});
	}
}
