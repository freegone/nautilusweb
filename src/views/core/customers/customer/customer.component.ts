import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of customer.
 *
 * @export
 * @class CustomerComponent
 */
@Component({
	selector: 'app-customer',
	templateUrl: './customer.component.html',
	styleUrls: [ './customer.component.scss' ]
})
export class CustomerComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof CustomerComponent
	 */
	public companies: Array<object>;
	/**
	 * Save customer object.
	 *
	 * @type {Array<object>}
	 * @memberof CustomerComponent
	 */
	public customer: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof CustomerComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof CustomerComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof CustomerComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of CustomerComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof CustomerComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.customer = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.customer ? this.customer.id : 0),
			companyId: new FormControl(this.customer ? this.customer.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.customer ? this.customer.officeId : '', [ Validators.required ]),
			name: new FormControl(this.customer ? this.customer.name : '', [
				Validators.required,
				Validators.maxLength(250)
			]),
			rfc: new FormControl(this.customer ? this.customer.rfc : '', [ Validators.maxLength(50) ]),
			email: new FormControl(this.customer ? this.customer.email : '', [ Validators.email, Validators.maxLength(150) ]),
			phone: new FormControl(this.customer ? this.customer.phone : '', [ Validators.maxLength(10) ]),
			address: new FormControl(this.customer ? this.customer.address : '', [ Validators.maxLength(250) ]),
			notes: new FormControl(this.customer ? this.customer.notes : '', [ Validators.maxLength(300) ])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof CustomerComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.customer ? [this.customer.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.customer ? [this.customer.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add customer in database.
	 *
	 * @memberof CustomerComponent
	 */
	public async saveCustomer(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Customers/UpdateCustomer', form);
			} else {
				response = await this.apiProvider.doPost('Customers/SaveCustomer', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof CustomerComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.customer) {
			form['id'] = this.customer.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete customer from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof CustomerComponent
	 */
	public async deleteCustomer(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el cliente ' + this.customer.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Customers/DeleteCustomer', this.customer.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof CustomerComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'customers' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
