/*Angular imports*/
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Regular imports*/
import Swal from 'sweetalert2';
import _ from 'underscore';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of user.
 *
 * @export
 * @class UserComponent
 */
@Component({
	selector: 'app-user',
	templateUrl: './user.component.html',
	styleUrls: [ './user.component.scss' ]
})
export class UserComponent implements OnInit {
	/**
	 * Save user object.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public user: any;
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public inputs: Array<object> = inputs;
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public selects: Array<object> = selects;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public form: FormGroup;
	/**
	 * Creates an instance of UserComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof UserComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.user = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.user ? this.user.id : 0),
			profileId: new FormControl(this.user ? [ this.user.profileId ] : '', [ Validators.required ]),
			companyId: new FormControl(this.user ? [ this.user.companyId ] : ''),
			officeId: new FormControl(this.user ? [ this.user.officeId ] : ''),
			name: new FormControl(this.user ? this.user.name : '', [
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(250)
			]),
			phone: new FormControl(this.user ? this.user.phone : '', [
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(15)
			]),
			email: new FormControl({ value: this.user ? this.user.email : '', disabled: this.user ? true : false }, [
				Validators.required,
				Validators.email,
				Validators.minLength(5),
				Validators.maxLength(150)
			]),
			status: new FormControl(this.user ? [ this.user.status ] : [ true ], [ Validators.required ]),
			picture: new FormControl(this.user ? this.user.picture : '')
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof UserComponent
	 */
	public async ngOnInit(): Promise<any> {
		this.myapp.setLoading(true);
		/* if (!this.user) {
			await this.searchId();
		} */
		await this.searchProfiles();
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Verify last Id.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchId(): Promise<any> {
		try {
			const id = await this.apiProvider.doGet('Users/LastId');
			this.form.controls.code.setValue(id);
		} catch (err) {}
	}
	/**
	 * Get profiles.
	 */
	public async searchProfiles(): Promise<any> {
		try {
			const profiles = await this.apiProvider.doGet('Profiles/GetProfiles');
			this.selects[1]['options'] = profiles['data'];
			this.form.controls.profileId.setValue(
				this.user ? [ this.user.profileId ] : [this.selects[1]['options'][0]['id']]
			);
		} catch (err) {}
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[2]['options'] = companies['data'];
			this.form.controls.companyId.setValue(
				this.user ? [ this.user.companyId ] : [this.selects[2]['options'][0]['id']]
			);
			/* if (this.user) {
				if (this.user.id_office.length > 0) {
					let index = -1;
					let i = 0;
					for (const option of this.selects[2]['options']) {
						for (const office of option.offices) {
							if (office.id === this.user.id_office[0]) {
								index = i;
							}
						}
						i++;
					}
					this.selects[3]['options'] = this.selects[2]['options'][index]['offices'];
					this.form.controls.id_office.setValue(this.user.id_office);
				} else {
					let index = -1;
					for (const option of this.selects[2]['options']) {
						index = _.findIndex(option.offices, { id: this.user.id_office[0] });
						if (index !== -1) {
							break;
						}
					}
					if (index !== -1) {
						this.selects[3]['options'] = this.selects[2]['options'][index]['offices'];
					} else {
						this.selects[3]['options'] = this.selects[2]['options'][0]['offices'];
					}
					this.form.controls.id_office.setValue([]);
				}
			} else {
				this.selects[3]['options'] = this.selects[2]['options'][0]['offices'];
				this.form.controls.id_office.setValue([]);
			} */
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[3]['options'] = offices['data'];
			const officeId = this.user ? this.user.officeId : [this.selects[4]['options'][0]['id']];
			console.log(officeId);
			this.form.controls.officeId.patchValue(officeId);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter here when upload file in input component.
	 *
	 * @param {*} file
	 * @memberof UserComponent
	 */
	public setImage(file): void {
		if (file) {
			if (file === 'default') {
				document.getElementById('img-usr')['src'] = '../../assets/img/avatar.png';
			} else {
				var reader = new FileReader();
				reader.onload = (e: any) => {
					document.getElementById('img-usr')['src'] = e.target.result;
				};
				reader.readAsDataURL(file);
			}
		}
	}
	/**
	 * Add user in database
	 *
	 * @memberof UserComponent
	 */
	public async addUser(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			if (form['id']) {
				await this.apiProvider.doPut('Users/UpdateUser', form);
			} else {
				await this.apiProvider.doPost('Users/SaveUser', form);
			}
			this.doCancel();
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values of Form Data.
	 *
	 * @private
	 * @returns {object}
	 * @memberof UserComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.user) {
			form['id'] = this.user.id;
		}
		for (const input of inputs) {
			if (input.name !== 'code') {
				form[input.name] = this.form.get(input.name).value;
			}
		}
		for (const select of selects) {
			form[select.name] = select.multiple
				? this.form.get(select.name).value
				: this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Changes offices from select.
	 *
	 * @memberof UserComponent
	 */
	public changeOffices(event): void {
		this.selects[3]['options'] = this.selects[2]['options'][event]['offices'];
		this.form.controls.id_office.setValue([]);
	}
	/**
	 * Delete user from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async deleteUser(): Promise<any> {
		Swal.fire({
			title:  `¿Estas seguro de eliminar al usuario ${this.user.name}? `,
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Users/DeleteUser', this.user.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
	 * Enter here when clicked cancel button.
	 *
	 * @memberof UserComponent
	 */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'users' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
