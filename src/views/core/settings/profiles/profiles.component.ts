/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relative imports*/
import _ from 'underscore';
import { ApiProvider } from '../../../../providers/providers.module';
import { AppComponent } from '../../../../app/app.component';
import { input } from './template.json';
/**
 * The component of profile.
 *
 * @export
 * @class ProfilesComponent
 */
@Component({
	selector: 'app-profiles',
	templateUrl: './profiles.component.html',
	styleUrls: [ './profiles.component.scss' ]
})
export class ProfilesComponent {
	/**
	 *  Local reference to array of profiles.
	 *
	 * @type {Array<object>}
	 * @memberof ProfilesComponent
	 */
	public profiles: Array<object> = [];
	/**
	 *  Local reference to array of profiles.
	 *
	 * @type {Array<object>}
	 * @memberof ProfilesComponent
	 */
	public originalProfiles: Array<object> = [];
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof ProfilesComponent
	 */
	public form: FormGroup;
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof ProfilesComponent
	 */
	public searchInput: object = input;
	/**
	 *Creates an instance of ProfilesComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ProfilesComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof ProfilesComponent
	 */
	public ngOnInit() {
		this.myapp.setLoading(true);
		this.profiles = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of users.
	 *
	 * @returns {Promise<any>}
	 * @memberof ProfilesComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const originalProfiles = await this.apiProvider.doGet('Profiles/GetProfiles');
			this.originalProfiles = originalProfiles['data'];
			this.profiles = _.clone(this.originalProfiles);
			/*let _profiles;
			for (let x = 0; x < this.profiles.length; x++) {
				_profiles = [];
				for (let i = 0; i < this.profiles[x]['permissions'].length; i++) {
					const _permissions = Object.values(this.profiles[x]['permissions'][i]);
					let flag = false;
					for (const _permission of _permissions) {
						if (_permission === 'true') {
							flag = true;
						}
					}
					if (flag) {
						_profiles.push(this.profiles[x]['permissions'][i]);
					}
				}
				this.profiles[x]['permissions'] = _profiles;
			}*/
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof ProfilesComponent
	 */
	public search(text): void {
		if (text === "") {
			this.profiles = this.originalProfiles;
		} else if (this.originalProfiles.length > 0) {
			this.profiles = this.originalProfiles.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @param {null} profile
	 * @memberof ProfilesComponent
	 */
	public goProfile(profile: null): void {
		this.router.navigate([ { outlets: { app: 'profile' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: profile
			}
		});
	}
}
