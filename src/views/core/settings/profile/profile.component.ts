/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import _ from 'underscore';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { web, mobile } from '../../app/modules.json';
import { inputs, checkboxs } from './template.json';
/**
 * The component of profile.
 *
 * @export
 * @class ProfileComponent
 */
@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: [ './profile.component.scss' ]
})
export class ProfileComponent {
	/**
	 * Save user object.
	 *
	 * @type {Array<object>}
	 * @memberof ProfileComponent
	 */
	public profile: any;
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof ProfileComponent
	 */
	public inputs: Array<object> = inputs;
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof ProfileComponent
	 */
	 public checkboxs: Array<object> = checkboxs;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof ProfileComponent
	 */
	public form: FormGroup;
	/**
	 * Array of modules.
	 *
	 * @type {Array<object>}
	 * @memberof ProfileComponent
	 */
	public _modules: Array<object>;
	/**
	 * Save position for show profile.
	 *
	 * @memberof ProfileComponent
	 */
	public _auxIndex;
	/**
	 *Creates an instance of ProfileComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ProfileComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.profile = this.router.getCurrentNavigation().extras.state.data;
		this._modules = web;
		this._auxIndex = 0;
		this.form = new FormGroup({
			id: new FormControl(this.profile ? this.profile.id : 0),
			name: new FormControl(this.profile ? this.profile.name : '', [
				Validators.required,
				Validators.minLength(1),
				Validators.maxLength(150)
			]),
			superUser: new FormControl(this.profile ? this.profile.superUser : false),
			access: new FormControl([ 1 ], [ Validators.required ])
		});
		const access = [ web ];
		for (const typeOfAccess of access) {
			for (const modules of typeOfAccess) {
				for (const _module of modules['submenu']) {
					const actions = [
						_module.id,
						_module.id + 'View',
						_module.id + 'Add',
						_module.id + 'Edit',
						_module.id + 'Delete'
					];
					for (let i = 0; i < actions.length; i++) {
						if (this.profile) {
							const permission = this.profile.access.web.permissions.find(
								(p) => p.moduleId === actions[0]
							);
							if (i !== 0) {
								if (i === 1) {
									this.form.addControl(actions[i.toString()], new FormControl(permission.actionView));
								} else if (i === 2) {
									this.form.addControl(actions[i.toString()], new FormControl(permission.actionAdd));
								} else if (i === 3) {
									this.form.addControl(actions[i.toString()], new FormControl(permission.actionEdit));
								} else if (i === 4) {
									this.form.addControl(actions[i.toString()], new FormControl(permission.actionDelete));
								}
							}
							this.form.addControl(actions[i.toString()], new FormControl(false));
						} else {
							this.form.addControl(actions[i.toString()], new FormControl(false));
						}
					}
				}
			}
		}
	}
	/**
   * Enter here when upload file in input component.
   *
   * @param {*} file
   * @memberof ProfileComponent
   */
	public setImage(file): void {
		if (file) {
			if (file === 'default') {
				document.getElementById('img-usr')['src'] = '../../assets/img/avatar.png';
			} else {
				var reader = new FileReader();
				reader.onload = (e: any) => {
					document.getElementById('img-usr')['src'] = e.target.result;
				};
				reader.readAsDataURL(file);
			}
		}
	}
	/**
	 * Enter here when change te profile in select.
	 *
	 * @param {*} type
	 * @memberof ProfileComponent
	 */
	public changeProfile(type): void {
		this._modules = type === 0 ? web : mobile;
		this._auxIndex = 0;
	}
	/**
	 * Enter here when click in p tag for change submodule permissions.
	 *
	 * @param {*} index
	 * @memberof ProfileComponent
	 */
	public setModule(index): void {
		this._auxIndex = index;
	}
	/**
	 * Add user in database
	 *
	 * @memberof ProfileComponent
	 */
	public async addProfile(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			if (form['id']) {
				await this.apiProvider.doPut('Profiles/UpdateProfile', form);
			} else {
				await this.apiProvider.doPost('Profiles/SaveProfile', form);
			}
			
			this.doCancel();
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	private getValues(): object {
		const form = {};
		if (this.profile) {
			form['id'] = this.profile.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const checkbox of checkboxs) {
			form[checkbox.name] = this.form.get(checkbox.name).value;
		}
		form['access'] = { web: { accessId: 1, permissions: [] }, mobile: { accessId: 2, permissions: [] } };
		const access = [ web, mobile ];
		let x = 0;
		for (const typeOfAccess of access) {
			for (const modules of typeOfAccess) {
				for (const _module of modules['submenu']) {
					const actions = [ 'View', 'Add', 'Edit', 'Delete' ];
					let _values = {
						moduleId: _module.id,
						actionView: false,
						actionAdd: false,
						actionEdit: false,
						actionDelete: false
					};
					for (const control of actions) {
						_values['action' + control] = this.form.controls[_module.id + control].value;
					}
					form['access'][x === 0 ? 'web' : 'mobile']['permissions'].push(_values);
				}
			}
			x++;
		}
		return form;
	}
	/**
	 * Delete profile from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof ProfileComponent
	 */
	public async deleteProfile(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar al perfil ' + this.profile.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					const response = await this.apiProvider.doDelete('Profiles/DeleteProfile', this.profile.id);
					if (response['data'] === null) {
						Swal.fire({
							type: 'error',
							title: 'Lo sentimos',
							confirmButtonColor: '#313731ad',
							text:
								'Esta perfil tiene usuarios asociados, remueve esta asociación para completar el proceso.'
						});
					} else {
						this.doCancel();
					}
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
	 * reset values when event is false.
	 *
	 * @param {*} event
	 * @param {*} name
	 * @memberof ProfileComponent
	 */
	public resetValues(value, id, action): void {
		if (!value && action === 'View') {
			const actions = [ 'Add', 'Edit', 'Delete' ];
			for (let i = 0; i < actions.length; i++) {
				if (this.form.get(id + actions[i]).value) {
					document.getElementById(id + actions[i]).click();
				}
			}
		}
		this.form.controls[id + action].setValue(value);
	}
	/**
   * Enter here when clicked cancel button.
   *
   * @memberof ProfileComponent
   */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'profiles' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
