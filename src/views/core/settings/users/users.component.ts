/*Angular imports*/
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relative imports*/
import { ApiProvider } from '../../../../providers/providers.module';
import { AppComponent } from '../../../../app/app.component';
import { input } from './template.json';
/**
 * The component of users.
 *
 * @export
 * @class UsersComponent
 */
@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: [ './users.component.scss' ]
})
export class UsersComponent implements OnInit {
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public users: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public originalUsers: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of UsersComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof UsersComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
		* The function to execute each time when enter to module.
		*/
	public ngOnInit() {
		this.myapp.setLoading(true);
		this.users = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of users.
	 *
	 * @returns {Promise<any>}
	 * @memberof UsersComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const users = await this.apiProvider.doGet('Users/GetUsers');
			this.users = users['data'];
			this.originalUsers = users['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof UsersComponent
	 */
	public search(text): void {
		if (text === "") {
			this.users = this.originalUsers;
		} else if (this.originalUsers.length > 0) {
			this.users = this.originalUsers.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof UsersComponent
	 */
	public goUser(user: null): void {
		this.router.navigate([ { outlets: { app: 'user' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: user
			}
		});
	}
}
