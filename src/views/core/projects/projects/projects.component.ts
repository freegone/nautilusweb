/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of projects.
 *
 * @export
 * @class ProjectsComponent
 */
@Component({
	selector: 'app-projects',
	templateUrl: './projects.component.html',
	styleUrls: [ './projects.component.scss' ]
})
export class ProjectsComponent {
	/**
	 * Local reference to array of projects.
	 *
	 * @type {Array<object>}
	 * @memberof ProjectsComponent
	 */
	public projects: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof ProjectsComponent
	 */
	public originalProjects: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof ProjectsComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof ProjectsComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of ProjectsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ProjectsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof ProjectsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.projects = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof ProjectsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const projects = await this.apiProvider.doGet('Projects/GetProjects/false');
			this.projects = projects['data'];
			this.originalProjects = projects['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof ProjectsComponent
	 */
	 public search(text): void {
		if (text === "") {
			this.projects = this.originalProjects;
		} else if (this.originalProjects.length > 0) {
			this.projects = this.originalProjects.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof ProjectsComponent
	 */
	public addProject(project = null): void {
		this.router.navigate([ { outlets: { app: 'project' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: project
			}
		});
	}
}
