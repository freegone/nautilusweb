/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of programs.
 *
 * @export
 * @class ProgramsComponent
 */
@Component({
	selector: 'app-programs',
	templateUrl: './programs.component.html',
	styleUrls: [ './programs.component.scss' ]
})
export class ProgramsComponent {
	/**
	 * Local reference to array of programs.
	 *
	 * @type {Array<object>}
	 * @memberof ProgramsComponent
	 */
	public programs: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof ProgramsComponent
	 */
	public originalPrograms: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof ProgramsComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof ProgramsComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of ProgramsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ProgramsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof ProgramsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.programs = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof ProgramsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const programs = await this.apiProvider.doGet('Programs/GetPrograms');
			this.programs = programs['data'];
			this.originalPrograms = programs['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof ProgramsComponent
	 */
	 public search(text): void {
		if (text === "") {
			this.programs = this.originalPrograms;
		} else if (this.originalPrograms.length > 0) {
			this.programs = this.originalPrograms.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof ProgramsComponent
	 */
	public addProgram(program = null): void {
		this.router.navigate([ { outlets: { app: 'program' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: program
			}
		});
	}
}
