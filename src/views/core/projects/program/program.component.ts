import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of program.
 *
 * @export
 * @class ProgramComponent
 */
@Component({
	selector: 'app-program',
	templateUrl: './program.component.html',
	styleUrls: [ './program.component.scss' ]
})
export class ProgramComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof ProgramComponent
	 */
	public companies: Array<object>;
	/**
	 * Save program object.
	 *
	 * @type {Array<object>}
	 * @memberof ProgramComponent
	 */
	public program: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof ProgramComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof ProgramComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof ProgramComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of ProgramComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ProgramComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.program = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.program ? this.program.id : 0),
			companyId: new FormControl(this.program ? this.program.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.program ? this.program.officeId : '', [ Validators.required ]),
			name: new FormControl(this.program ? this.program.name : '', [ Validators.required, Validators.maxLength(250) ])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof ProgramComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.program ? [this.program.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.program ? [this.program.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add program in database.
	 *
	 * @memberof ProgramComponent
	 */
	public async saveProgram(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Programs/UpdateProgram', form);
			} else {
				response = await this.apiProvider.doPost('Programs/SaveProgram', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof ProgramComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.program) {
			form['id'] = this.program.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete program from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof ProgramComponent
	 */
	public async deleteProgram(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el programa ' + this.program.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Programs/DeleteProgram', this.program.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof ProgramComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'programs' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
