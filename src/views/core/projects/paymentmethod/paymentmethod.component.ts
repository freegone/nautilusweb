import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
import Swal from 'sweetalert2';
/**
 * The component of paymentMethod.
 *
 * @export
 * @class PaymentMethodComponent
 */
@Component({
	selector: 'app-paymentmethod',
	templateUrl: './paymentmethod.component.html',
	styleUrls: [ './paymentmethod.component.scss' ]
})
export class PaymentMethodComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof PaymentMethodComponent
	 */
	public companies: Array<object>;
	/**
	 * Save paymentMethod object.
	 *
	 * @type {Array<object>}
	 * @memberof PaymentMethodComponent
	 */
	public paymentMethod: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof PaymentMethodComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof PaymentMethodComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof PaymentMethodComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of PaymentMethodComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof PaymentMethodComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.paymentMethod = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.paymentMethod ? this.paymentMethod.id : 0),
			companyId: new FormControl(this.paymentMethod ? this.paymentMethod.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.paymentMethod ? this.paymentMethod.officeId : '', [ Validators.required ]),
			name: new FormControl(this.paymentMethod ? this.paymentMethod.name : '', [ Validators.required, Validators.maxLength(250) ])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof PaymentMethodComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.paymentMethod ? [this.paymentMethod.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.paymentMethod ? [this.paymentMethod.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add paymentMethod in database.
	 *
	 * @memberof PaymentMethodComponent
	 */
	public async savePaymentMethod(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('paymentMethods/UpdatepaymentMethod', form);
			} else {
				response = await this.apiProvider.doPost('paymentMethods/SavepaymentMethod', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof PaymentMethodComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.paymentMethod) {
			form['id'] = this.paymentMethod.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete paymentMethod from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof PaymentMethodComponent
	 */
	public async deletePaymentMethod(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar la forma de pago ' + this.paymentMethod.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('paymentMethods/DeletepaymentMethod', this.paymentMethod.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof PaymentMethodComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'paymentMethods' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
