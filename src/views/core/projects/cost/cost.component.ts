import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of cost.
 *
 * @export
 * @class CostComponent
 */
@Component({
	selector: 'app-cost',
	templateUrl: './cost.component.html',
	styleUrls: [ './cost.component.scss' ]
})
export class CostComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof CostComponent
	 */
	public companies: Array<object>;
	/**
	 * Save cost object.
	 *
	 * @type {Array<object>}
	 * @memberof CostComponent
	 */
	public cost: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof CostComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof CostComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof CostComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of CostComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof CostComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.cost = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.cost ? this.cost.id : 0),
			companyId: new FormControl(this.cost ? this.cost.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.cost ? this.cost.officeId : '', [ Validators.required ]),
			level1: new FormControl(this.cost ? this.cost.level1 : '', [ Validators.required, Validators.maxLength(250) ]),
			level2: new FormControl(this.cost ? this.cost.level2 : '', [ Validators.required, Validators.maxLength(250) ]),
			level3: new FormControl(this.cost ? this.cost.level3 : '', [ Validators.required, Validators.maxLength(250) ]),
			level4: new FormControl(this.cost ? this.cost.level4 : '', [ Validators.required, Validators.maxLength(250) ])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof CostComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.cost ? [this.cost.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.cost ? [this.cost.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add cost in database.
	 *
	 * @memberof CostComponent
	 */
	public async saveCost(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Costs/UpdateCost', form);
			} else {
				response = await this.apiProvider.doPost('Costs/SaveCost', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof CostComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.cost) {
			form['id'] = this.cost.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete cost from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof CostComponent
	 */
	public async deleteCost(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el costo de nivel 1 ' + this.cost.level1 + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Costs/DeleteCost', this.cost.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof CostComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'costs' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
