import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of project.
 *
 * @export
 * @class ProjectComponent
 */
@Component({
	selector: 'app-project',
	templateUrl: './project.component.html',
	styleUrls: [ './project.component.scss' ]
})
export class ProjectComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof ProjectComponent
	 */
	public companies: Array<object>;
	/**
	 * Save project object.
	 *
	 * @type {Array<object>}
	 * @memberof ProjectComponent
	 */
	public project: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof ProjectComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof ProjectComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof ProjectComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of ProjectComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ProjectComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.project = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.project ? this.project.id : 0),
			companyId: new FormControl(this.project ? this.project.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.project ? this.project.officeId : '', [ Validators.required ]),
			name: new FormControl(this.project ? this.project.name : '', [ Validators.required, Validators.maxLength(250) ]),
			units: new FormControl(this.project ? this.project.units : '', [ Validators.maxLength(150) ])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof ProjectComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.project ? [this.project.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.project ? [this.project.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add project in database.
	 *
	 * @memberof ProjectComponent
	 */
	public async saveProject(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Projects/UpdateProject', form);
			} else {
				response = await this.apiProvider.doPost('Projects/SaveProject', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof ProjectComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.project) {
			form['id'] = this.project.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete project from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof ProjectComponent
	 */
	public async deleteProject(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar este projecto ' + this.project.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Projects/DeleteProject', this.project.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof ProjectComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'projects' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
