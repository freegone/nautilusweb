import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of agent.
 *
 * @export
 * @class AgentComponent
 */
@Component({
	selector: 'app-agent',
	templateUrl: './agent.component.html',
	styleUrls: [ './agent.component.scss' ]
})
export class AgentComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof AgentComponent
	 */
	public companies: Array<object>;
	/**
	 * Save agent object.
	 *
	 * @type {Array<object>}
	 * @memberof AgentComponent
	 */
	public agent: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof AgentComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof AgentComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof AgentComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of AgentComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof AgentComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.agent = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.agent ? this.agent.id : 0),
			companyId: new FormControl(this.agent ? this.agent.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.agent ? this.agent.officeId : '', [ Validators.required ]),
			name: new FormControl(this.agent ? this.agent.name : '', [ Validators.required, Validators.maxLength(250) ]),
			rfc: new FormControl(this.agent ? this.agent.rfc : '', [ Validators.maxLength(50) ]),
			phone: new FormControl(this.agent ? this.agent.phone : '', [
				Validators.required,
				Validators.minLength(3),
				Validators.maxLength(15)
			]),
			percentaje: new FormControl(this.agent ? this.agent.percentaje : '', [ Validators.required, Validators.maxLength(50) ]),
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof AgentComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.agent ? [this.agent.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof UserComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.agent ? [this.agent.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add agent in database.
	 *
	 * @memberof AgentComponent
	 */
	public async saveAgent(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Agents/UpdateAgent', form);
			} else {
				response = await this.apiProvider.doPost('Agents/SaveAgent', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof AgentComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.agent) {
			form['id'] = this.agent.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete agent from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof AgentComponent
	 */
	public async deleteAgent(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el agente ' + this.agent.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Agents/DeleteAgent', this.agent.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof AgentComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'agents' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
