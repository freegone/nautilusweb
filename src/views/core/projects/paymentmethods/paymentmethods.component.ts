/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of paymentMethods.
 *
 * @export
 * @class paymentMethodsComponent
 */
@Component({
	selector: 'app-paymentmethods',
	templateUrl: './paymentmethods.component.html',
	styleUrls: [ './paymentmethods.component.scss' ]
})
export class PaymentMethodsComponent {
	/**
	 * Local reference to array of paymentMethods.
	 *
	 * @type {Array<object>}
	 * @memberof PaymentMethodsComponent
	 */
	public paymentMethods: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof PaymentMethodsComponent
	 */
	public originalPaymentMethods: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof PaymentMethodsComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof PaymentMethodsComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of PaymentMethodsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof PaymentMethodsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof PaymentMethodsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.paymentMethods = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof PaymentMethodsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const paymentMethods = await this.apiProvider.doGet('PaymentMethods/GetPaymentMethods');
			this.paymentMethods = paymentMethods['data'];
			this.originalPaymentMethods = paymentMethods['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof PaymentMethodsComponent
	 */
	 public search(text): void {
		if (text === "") {
			this.paymentMethods = this.originalPaymentMethods;
		} else if (this.originalPaymentMethods.length > 0) {
			this.paymentMethods = this.originalPaymentMethods.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof PaymentMethodsComponent
   	*/
	public addPaymentMethod(paymentMethod = null): void {
		this.router.navigate([ { outlets: { app: 'paymentMethod' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: paymentMethod
			}
		});
	}
}
