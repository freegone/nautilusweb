/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
/**
 * The component of costs.
 *
 * @export
 * @class CostsComponent
 */
@Component({
	selector: 'app-costs',
	templateUrl: './costs.component.html',
	styleUrls: [ './costs.component.scss' ]
})
export class CostsComponent {
	/**
	 * Local reference to array of costs.
	 *
	 * @type {Array<object>}
	 * @memberof CostsComponent
	 */
	public costs: Array<object> = [];
	/**
	 *Creates an instance of CostsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof CostsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof CostsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.costs = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof CostsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const costs = await this.apiProvider.doGet('Costs/GetCosts');
			this.costs = costs['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof CostsComponent
	 */
	public addCosts(cost = null): void {
		this.router.navigate([ { outlets: { app: 'cost' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: cost
			}
		});
	}
}
