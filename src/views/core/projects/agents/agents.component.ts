/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of agents.
 *
 * @export
 * @class AgentsComponent
 */
@Component({
	selector: 'app-agents',
	templateUrl: './agents.component.html',
	styleUrls: [ './agents.component.scss' ]
})
export class AgentsComponent {
	/**
	 * Local reference to array of agents.
	 *
	 * @type {Array<object>}
	 * @memberof AgentsComponent
	 */
	public agents: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public originalAgents: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof UserComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of AgentsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof AgentsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof AgentsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.agents = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof AgentsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const agents = await this.apiProvider.doGet('Agents/GetAgents');
			this.agents = agents['data'];
			this.originalAgents = agents['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof AgentsComponent
	 */
	public search(text): void {
		if (text === "") {
			this.agents = this.originalAgents;
		} else if (this.originalAgents.length > 0) {
			this.agents = this.originalAgents.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof AgentsComponent
	 */
	public addAgent(agent = null): void {
		this.router.navigate([ { outlets: { app: 'agent' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: agent
			}
		});
	}
}
