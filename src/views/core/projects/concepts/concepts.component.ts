/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of concepts.
 *
 * @export
 * @class ConceptsComponent
 */
@Component({
	selector: 'app-concepts',
	templateUrl: './concepts.component.html',
	styleUrls: [ './concepts.component.scss' ]
})
export class ConceptsComponent {
	/**
	 * Local reference to array of concepts.
	 *
	 * @type {Array<object>}
	 * @memberof ConceptsComponent
	 */
	public concepts: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof ConceptsComponent
	 */
	public originalConcepts: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof ConceptsComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof ConceptsComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of ConceptsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ConceptsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof ConceptsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.concepts = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof ConceptsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const concepts = await this.apiProvider.doGet('Concepts/GetConcepts');
			this.concepts = concepts['data'];
			this.originalConcepts = concepts['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof ConceptsComponent
	 */
	public search(text): void {
		if (text === "") {
			this.concepts = this.originalConcepts;
		} else if (this.originalConcepts.length > 0) {
			this.concepts = this.originalConcepts.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof ConceptsComponent
	 */
	public addConcept(program = null): void {
		this.router.navigate([ { outlets: { app: 'concept' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: program
			}
		});
	}
}
