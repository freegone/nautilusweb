/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of departments.
 *
 * @export
 * @class DepartmentsComponent
 */
@Component({
	selector: 'app-departments',
	templateUrl: './departments.component.html',
	styleUrls: [ './departments.component.scss' ]
})
export class DepartmentsComponent {
	/**
	 * Local reference to array of departments.
	 *
	 * @type {Array<object>}
	 * @memberof DepartmentsComponent
	 */
	public departments: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof DepartmentsComponent
	 */
	public originalDepartments: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof DepartmentsComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof DepartmentsComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of DepartmentsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof DepartmentsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof DepartmentsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.departments = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof DepartmentsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const departments = await this.apiProvider.doGet('Departments/GetDepartments');
			this.departments = departments['data'];
			this.originalDepartments = departments['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof DepartmentsComponent
	 */
	public search(text): void {
		if (text === "") {
			this.departments = this.originalDepartments;
		} else if (this.originalDepartments.length > 0) {
			this.departments = this.originalDepartments.filter(x => x["unit"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof DepartmentsComponent
	 */
	public addDepartment(department = null): void {
		this.router.navigate([ { outlets: { app: 'department' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: department
			}
		});
	}
}
