import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of department.
 *
 * @export
 * @class DepartmentComponent
 */
@Component({
	selector: 'app-department',
	templateUrl: './department.component.html',
	styleUrls: [ './department.component.scss' ]
})
export class DepartmentComponent implements OnInit {
	/**
	 * Save companies array.
	 *
	 * @type {Array<object>}
	 * @memberof DepartmentComponent
	 */
	public companies: Array<object>;
	/**
	 * Save department object.
	 *
	 * @type {Array<object>}
	 * @memberof DepartmentComponent
	 */
	public department: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof DepartmentComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof DepartmentComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof DepartmentComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of DepartmentComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof DepartmentComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.department = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.department ? this.department.id : 0),
			companyId: new FormControl(this.department ? this.department.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.department ? this.department.officeId : '', [ Validators.required ]),
			projectId: new FormControl(this.department ? this.department.projectId : '', [ Validators.required ]),
			unit: new FormControl(this.department ? this.department.unit : '', [ Validators.required, Validators.maxLength(250) ]),
			type: new FormControl(this.department ? this.department.type : '', [ Validators.required, Validators.maxLength(250) ]),
			description: new FormControl(this.department ? this.department.description : '', [ Validators.required, Validators.maxLength(250) ]),
			salePrice: new FormControl(this.department ? this.department.salePrice : '', [ Validators.required, Validators.maxLength(250) ]),
			characteristics: new FormControl(this.department ? this.department.characteristics : '', [ Validators.required, Validators.maxLength(250) ])//,
			//status: new FormControl(this.department ? [ this.department.status ] : [ 1 ], [ Validators.required ]),
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof DepartmentComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
		this.searchProjects();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof DepartmentComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.department ? [this.department.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof DepartmentComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.department ? [this.department.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get projects.
	 *
	 * @returns {Promise<any>}
	 * @memberof DepartmentComponent
	 */
	 public async searchProjects(): Promise<any> {
		try {
			const projects = await this.apiProvider.doGet('Projects/GetProjects/false');
			this.selects[2]['options'] = projects['data'];
			this.form.controls.projectId.setValue(this.department ? [this.department.projectId] : [this.selects[2]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add department in database.
	 *
	 * @memberof DepartmentComponent
	 */
	public async saveDepartment(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Departments/UpdateDepartment', form);
			} else {
				response = await this.apiProvider.doPost('Departments/SaveDepartment', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof DepartmentComponent
	 */
	private getValues(): object {
		const form = {};
		if (this.department) {
			form['id'] = this.department.id;
		}
		for (const input of inputs) {
			form[input.name] = String(this.form.get(input.name).value);
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete department from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof DepartmentComponent
	 */
	public async deleteDepartment(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el departmento ' + this.department.type + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Departments/DeleteDepartment', this.department.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
  * Enter here when clicked cancel button.
  *
  * @memberof DepartmentComponent
  */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'departments' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
