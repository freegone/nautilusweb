import { OnInit } from '@angular/core';
/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects } from './template.json';
/**
 * The component of concept.
 *
 * @export
 * @class ConceptComponent
 */
@Component({
	selector: 'app-concept',
	templateUrl: './concept.component.html',
	styleUrls: [ './concept.component.scss' ]
})
export class ConceptComponent implements OnInit {
	/**
	 * Save concept object.
	 *
	 * @type {Array<object>}
	 * @memberof ConceptComponent
	 */
	public concept: any;
	/**
   *. Inputs template.
   *
   * @type {Array<object>}
   * @memberof ConceptComponent
   */
	public inputs: Array<object> = inputs;
	/**
   *. Selects template.
   *
   * @type {Array<object>}
   * @memberof ConceptComponent
   */
	public selects: Array<object> = selects;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof ConceptComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of ConceptComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ConceptComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.concept = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.concept ? this.concept.id : 0),
			companyId: new FormControl(this.concept ? this.concept.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.concept ? this.concept.officeId : '', [ Validators.required ]),
			type: new FormControl(this.concept ? [ this.concept.type ] : [ 1 ], [ Validators.required ]),
			name: new FormControl(this.concept ? this.concept.name : '', [ Validators.required, Validators.maxLength(250) ]),
			subName: new FormControl(this.concept ? this.concept.subName : '', [ Validators.maxLength(250) ]),
			subConcepts: new FormControl(this.concept ? this.concept.subConcepts : [], [ Validators.maxLength(250) ])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof ConceptComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
		this.setDisabledSubConcept();
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof ConceptComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.concept ? [this.concept.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof ConceptComponent
	 */
	 public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.concept ? [this.concept.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Set disable sub concept input.
	 *
	 * @returns {Promise<any>}
	 * @memberof ConceptComponent
	 */
	private setDisabledSubConcept(): void {
		if (this.form.controls.type.value[0] === 0) {
			this.inputs[1]['class'] = "";
		} else {
			this.form.controls.subName.setValue("");
			this.form.controls.subConcepts.setValue([]);
			this.inputs[1]['class'] = "disabled";
		}
	}
	/**
	 * Add concept in database.
	 *
	 * @memberof ConceptComponent
	 */
	public async saveConcept(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Concepts/UpdateConcept', form);
			} else {
				response = await this.apiProvider.doPost('Concepts/SaveConcept', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof ConceptComponent
	 */
	private getValues(): object {
		const form = {
			'subConcepts': this.form.controls.subConcepts.value.map(x => x.name)
		};

		if (this.concept) {
			form['id'] = this.concept.id;
		}
		for (const input of inputs) {
			form[input.name] = this.form.get(input.name).value;
		}
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];
		}
		return form;
	}
	/**
	 * Delete concept from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof ConceptComponent
	 */
	public async deleteConcept(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el concepto ' + this.concept.name + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Concepts/DeleteConcept', this.concept.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
	 * Enter here when clicked cancel button.
	 *
	 * @memberof ConceptComponent
	 */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'concepts' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
	/**
	 * Select change by option.
	 *
	 * @memberof ConceptComponent
	 */
	public selectChange(option): void {
		if (option.formControlName === "type") {
			this.setDisabledSubConcept();
		}
	}
	/**
	 * Add sub concept.
	 *
	 * @memberof ConceptComponent
	 */
	public addSubConcept()
	{
		const subName = this.form.controls.subName.value;
		if (subName) {
			let subConcepts = this.form.controls.subConcepts.value;
			subConcepts.push({ name: subName });
			this.form.controls.subConcepts.setValue(subConcepts);
			this.form.controls.subName.setValue("");
		}
	}

	/**
	 * Remove sub concept.
	 *
	 * @memberof ConceptComponent
	 */
	public removeSubConcept(index)
	{
		const subConcepts = this.form.controls.subConcepts.value;

		if (subConcepts[index]) {
			var newSubConcepts = subConcepts.filter(x => x !== subConcepts[index]);
			this.form.controls.subConcepts.setValue(newSubConcepts);
		}
	}
}
