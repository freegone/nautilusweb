/*Angular imports*/
import { Component, OnInit, AfterViewInit } from '@angular/core';
/*Relative imports*/
import Chart from 'chart.js';
import { AppComponent } from '../../../app/app.component';
import { ApiProvider } from '../../../providers/providers.module';
import { template } from './template.json';
/**
 * The component of home.
 *
 * @export
 * @class HomeComponent
 * @implements {AfterViewInit}
 */
@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: [ './home.component.scss' ]
})
export class HomeComponent implements AfterViewInit, OnInit {
	/**
	 * Save the Html template.
	 *
	 * @type {object}
	 * @memberof HomeComponent
	 */
	public _template: object = template;
	/**
	 * Creates an instance of HomeComponent.
	 * @param {ApiProvider} apiProvider
	 * @memberof HomeComponent
	 */
	constructor(private apiProvider: ApiProvider, public myapp: AppComponent) {}
	public async ngOnInit() {
		this.myapp.setLoading(true);
		const user = JSON.parse(localStorage.getItem('nautilusSession'));
		try {
			const overview = await this.apiProvider.doGet('Home/GetOverview');
			this._template['header'][0].id = overview.data.users;
			this._template['header'][1].id = overview.data.agents;
			this._template['header'][2].id = overview.data.companies;
			this._template['header'][3].id = overview.data.offices;
			const profile = await this.apiProvider.doGet('Profiles/GetProfileByUserId?id=' + user.id);
			this.myapp.setProfile(profile['data']);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * LifeCycle Event execute after render the Dom.
	 *
	 * @memberof HomeComponent
	 */
	public ngAfterViewInit(): void {
		let sales: any = document.getElementById('sales');
		let expenses: any = document.getElementById('expenses');
		let budgets: any = document.getElementById('budgets');
		new Chart(sales.getContext('2d'), {
			type: 'line',
			data: {
				labels: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
				datasets: [
					{
						label: 'Enero',
						fill: true,
						lineTension: 0.3,
						backgroundColor: 'rgba(75,192,192,0.4)',
						borderColor: 'rgba(75,192,192,1)',
						borderCapStyle: 'butt',
						borderDash: [],
						borderDashOffset: 0.0,
						borderJoinStyle: 'miter',
						pointBorderColor: 'rgba(75,192,192,1)',
						pointBackgroundColor: '#fff',
						pointBorderWidth: 1,
						pointHoverRadius: 5,
						pointHoverBackgroundColor: 'rgba(75,192,192,1)',
						pointHoverBorderColor: 'rgba(220,220,220,1)',
						pointHoverBorderWidth: 2,
						pointRadius: 1,
						pointHitRadius: 10,
						data: [ 65, 59, 80, 81, 56, 55, 40, 38, 22, 10, 22, 5 ],
						spanGaps: false
					},
					{
						label: 'Febrero',
						fill: false,
						lineTension: 0.3,
						backgroundColor: 'rgba(169,68,66,0.98)',
						borderColor: 'rgba(169,68,66,0.98)',
						borderCapStyle: 'butt',
						borderDash: [],
						borderDashOffset: 0.0,
						borderJoinStyle: 'miter',
						pointBorderColor: 'rgba(169,68,66,0.98)',
						pointBackgroundColor: '#fff',
						pointBorderWidth: 1,
						pointHoverRadius: 5,
						pointHoverBackgroundColor: 'rgba(169,68,66,0.98)',
						pointHoverBorderColor: 'rgba(220,220,220,1)',
						pointHoverBorderWidth: 2,
						pointRadius: 1,
						pointHitRadius: 10,
						data: [ 60, 79, 90, 61, 76, 56, 80, 22, 11, 43, 22, 43],
						spanGaps: false
					}
				]
			},
			options: {
				maintainAspectRatio: false,
				responsive: false,
				legend: {
					position: 'top'
				}
			}
		});
		new Chart(expenses.getContext('2d'), {
			type: 'pie',
			data: {
				labels: [ 'Egreso', 'Contrato'],
				datasets: [
					{
						label: '# of Votes',
						data: [ 1, 2 ],
						backgroundColor: [ 'rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)' ],
						borderWidth: 1
					}
				]
			},
			options: {
				maintainAspectRatio: false,
				responsive: false,
				legend: {
					position: 'top'
				}
			}
		});
		new Chart(budgets.getContext('2d'), {
			type: 'pie',
			data: {
				labels: [ 'Ingresos', 'Gastos'],
				datasets: [
					{
						label: '# of Votes',
						data: [ 1, 2 ],
						backgroundColor: [ 'rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)' ],
						borderWidth: 1
					}
				]
			},
			options: {
				maintainAspectRatio: false,
				responsive: false,
				legend: {
					position: 'top'
				}
			}
		});
	}
}
