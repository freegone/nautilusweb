/*Angular imports*/
import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import moment, { relativeTimeThreshold } from 'moment';
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects, secondSelects, table } from './template.json';
var formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD'
  });
/**
 * The component of sale.
 *
 * @export
 * @class SaleComponent
 */
@Component({
	selector: 'app-sale',
	templateUrl: './sale.component.html',
	styleUrls: [ './sale.component.scss' ]
})
export class SaleComponent implements OnInit {
	/**
	 * Save sale object.
	 *
	 * @type {Array<object>}
	 * @memberof SaleComponent
	 */
	public sale: any;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof SaleComponent
	 */
	 public inputs: Array<object> = inputs;
	/**
	 * Selects template.
	 *
	 * @type {Array<object>}
	 * @memberof SaleComponent
	 */
	public selects: Array<object> = selects;
	/**
	 * Selects template.
	 *
	 * @type {Array<object>}
	 * @memberof SaleComponent
	 */
	public selectsSecond: Array<object> = secondSelects;
	/**
	 * Table template.
	 *
	 * @type {Array<object>}
	 * @memberof SaleComponent
	 */
	 public table: object = table;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof SaleComponent
	 */
	public form: FormGroup;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof SaleComponent
	 */
	public editIndex: number = null;
	/**
	 *Creates an instance of SaleComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof SaleComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.sale = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.sale ? this.sale.id : 0),
			companyId: new FormControl(this.sale ? this.sale.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.sale ? this.sale.officeId : '', [ Validators.required ]),
			projectId: new FormControl(this.sale ? this.sale.projectId : '', [ Validators.required ]),
			departmentType: new FormControl(this.sale ? this.sale.departmentType : '', [ Validators.required ]),
			customerId: new FormControl(this.sale ? this.sale.customerId : '', [ Validators.required ]),
			agentId: new FormControl(this.sale ? this.sale.agentId : '', [ Validators.required ]),
			scheme: new FormControl(this.sale ? this.sale.scheme : '', [ Validators.required ]),
			contractDate: new FormControl(this.sale ? moment(this.sale.contractDate).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'), [ Validators.required ]),
			salePrice: new FormControl(this.sale ? this.sale.salePrice : '', [ Validators.required, Validators.maxLength(50) ]),
			paid: new FormControl(this.sale ? this.sale.paid : 0, [ Validators.required, Validators.maxLength(50) ]),
			residue: new FormControl(this.sale ? this.sale.residue : 0, [ Validators.required, Validators.maxLength(50) ]),
			percentage: new FormControl(this.sale ? parseFloat(this.sale.percentage) : 0, [ Validators.required, Validators.maxLength(3) ]),
			percentageAgent: new FormControl(this.sale ? this.sale.percentageAgent : '', [ Validators.required, Validators.maxLength(3) ]),
			unit: new FormControl(this.sale ? this.sale.unit : '', [ Validators.required, Validators.maxLength(250) ]),
			departmentId: new FormControl(this.sale ? this.sale.departmentId : '', [ Validators.required, Validators.maxLength(250) ]),
			comisionAgent: new FormControl(this.sale ? this.sale.comisionAgent : '', [ Validators.required, Validators.maxLength(250) ]),
			programId: new FormControl(this.sale ? this.sale.programId : '', [ Validators.required ]),
			paidDate: new FormControl(moment().format('YYYY-MM-DD'), [ Validators.required ]),
			paidImport: new FormControl('00.00', [ Validators.maxLength(50) ]),
			conceptId: new FormControl([]),
			statusId: new FormControl([ 1 ]),
			paymentMethodId: new FormControl([]),
			sales: new FormControl([])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof SaleComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchOffices();
		this.searchProjects();
		//this.searchDepartments();
		this.searchPaymentMethods();
		this.searchCustomers();
		this.searchAgents();
		this.searchConcepts();
		this.searchPrograms();

		if (this.sale) {
			this.setDisabledInputs();
		} else {
			this.setEnabledInputs();
		}

		setTimeout(() => {
			this.form.controls.sales.setValue(this.getSales());
		}, 100);
	}
	/**
	 * Set disable sub concept input.
	 *
	 * @returns {Promise<any>}
	 * @memberof ConceptComponent
	 */
	private setDisabledInputs(): void {
		this.selects[0]['disabled'] = true;
		this.selects[1]['disabled'] = true;
		this.selects[2]['disabled'] = true;
		this.selects[3]['disabled'] = true;
		this.selects[3]['inputs'][1]['class'] = "disabled";
		this.selects[3]['inputs'][2]['class'] = "disabled";
		this.selects[4]['disabled'] = true;
		this.selects[4]['inputs'][0]['class'] = "disabled";
		this.selects[5]['disabled'] = true;
		this.selects[5]['inputs'][0]['class'] = "disabled";
		this.selects[6]['disabled'] = true;
	}
	/**
	 * Set disable sub concept input.
	 *
	 * @returns {Promise<any>}
	 * @memberof ConceptComponent
	 */
	private setEnabledInputs(): void {
		this.selects[0]['disabled'] = false;
		this.selects[1]['disabled'] = false;
		this.selects[2]['disabled'] = false;
		this.selects[3]['disabled'] = false;
		this.selects[3]['inputs'][1]['class'] = "";
		this.selects[3]['inputs'][2]['class'] = "";
		this.selects[4]['disabled'] = false;
		this.selects[4]['inputs'][0]['class'] = "";
		this.selects[5]['disabled'] = false;
		this.selects[5]['inputs'][0]['class'] = "";
		this.selects[6]['disabled'] = false;
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			this.form.controls.companyId.setValue(this.sale ? [this.sale.companyId] : [this.selects[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchOffices(): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.selects[1]['options'] = offices['data'];
			this.form.controls.officeId.setValue(this.sale ? [this.sale.officeId] : [this.selects[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get projects.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	 public async searchProjects(): Promise<any> {
		try {
			const projects = await this.apiProvider.doGet(this.sale ? 'Projects/GetProjects/true' : 'Projects/GetProjects/false');
			this.selects[2]['options'] = projects['data'];
			this.form.controls.projectId.setValue(this.sale ? [this.sale.projectId] : [this.selects[2]['options'][0]['id']]);
			this.selects[3]['options'] = this.selects[2]['options'][this.getProjectIndex()]['departments'].map(x => ({id: x.unit, name: x.unit, type: x.type, departmentId: x.id, salePrice: x.salePrice}));
			this.form.controls.unit.setValue([this.getUnit()]);
			this.form.controls.departmentId.setValue(this.sale ? [this.sale.departmentId] : [this.selects[2]['options'][0]['departments'][0]['id']]);
			this.form.controls.salePrice.setValue(this.sale ? this.sale.salePrice : this.selects[2]['options'][0]['departments'][0].salePrice);
			this.form.controls.departmentType.setValue(this.getDepartmentType());
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get unit Index.
	 *
	 * @returns {number}
	 * @memberof SaleComponent
	 */
	public getProjectIndex(): number {
		if (this.sale) {
			const projects = this.selects[2]['options'];
			const index = projects.findIndex(x => x.id === this.sale.projectId);
			return index;
		}

		return 0;
	}
	/**
	 * Get unit.
	 *
	 * @returns {string}
	 * @memberof SaleComponent
	 */
	public getUnit(): string {
		if (this.sale) {
			const indexProject = this.getProjectIndex();
			const indexDepartment = this.selects[2]['options'][indexProject]['departments'].findIndex(x => x.id === this.sale.departmentId);
			return this.selects[2]['options'][indexProject]['departments'][indexDepartment].unit;
		}

		return this.selects[2]['options'][0]['departments'][0].unit;
	}
	/**
	 * Get department type.
	 *
	 * @returns {string}
	 * @memberof SaleComponent
	 */
	public getDepartmentType(): string {
		if (this.sale) {
			const indexProject = this.getProjectIndex();
			const indexDepartment = this.selects[2]['options'][indexProject]['departments'].findIndex(x => x.id === this.sale.departmentId);

			return this.selects[2]['options'][indexProject]['departments'][indexDepartment].type;
		}

		return this.selects[2]['options'][0]['departments'][0].type;
	}
	/**
	 * Get departments.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchDepartments(): Promise<any> {
		try {
			const departments = await this.apiProvider.doGet('Departments/GetDepartments');
			departments['data'] = departments['data'].map(x => ({ ...x, name: x.type }));
			this.selects[4]['options'] = departments['data'];
			this.form.controls.departmentType.setValue(this.sale ? [this.sale.departmentType] : [this.selects[4]['options'][0]['id']]);
			this.form.controls.salePrice.setValue(this.sale ? this.sale.salePrice : [this.selects[4]['options'][0]['salePrice']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get payment methods.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchPaymentMethods(): Promise<any> {
		try {
			const paymentMethods = await this.apiProvider.doGet('PaymentMethods/GetPaymentMethods');
			this.selectsSecond[1]['options'] = paymentMethods['data'];
			this.form.controls.paymentMethodId.setValue([this.selectsSecond[1]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get customers.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchCustomers(): Promise<any> {
		try {
			const customers = await this.apiProvider.doGet('Customers/GetCustomers');
			this.selects[4]['options'] = customers['data'];
			this.form.controls.customerId.setValue(this.sale ? [this.sale.customerId] : [this.selects[4]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get agents.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchAgents(): Promise<any> {
		try {
			const agents = await this.apiProvider.doGet('Agents/GetAgents');
			this.selects[5]['options'] = agents['data'];
			this.form.controls.agentId.setValue(this.sale ? [this.sale.agentId] : [this.selects[5]['options'][0]['id']]);
			this.form.controls.percentageAgent.setValue(this.sale ? this.sale.percentageAgent : [this.selects[5]['options'][0]['percentaje']]);
			this.calculateTotalComisionAgent(this.sale ? this.sale.percentageAgent : this.selects[5]['options'][0]['percentaje']);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get concepts.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchConcepts(): Promise<any> {
		try {
			const concepts = await this.apiProvider.doGet('Concepts/GetConcepts');
			this.selectsSecond[0]['options'] = concepts['data'];
			this.form.controls.conceptId.setValue([this.selectsSecond[0]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get programs.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async searchPrograms(): Promise<any> {
		try {
			const programs = await this.apiProvider.doGet('Programs/GetPrograms');
			this.selects[6]['options'] = programs['data'];
			this.form.controls.programId.setValue(this.sale ? [this.sale.programId] : [this.selects[6]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Add sale in database.
	 *
	 * @memberof SaleComponent
	 */
	public async saveSale(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Sales/UpdateSale', form);
			} else {
				response = await this.apiProvider.doPost('Sales/SaveSale', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof SaleComponent
	 */
	private getValues(): object {
		const form = {};

		if (this.sale) {
			form['id'] = this.sale.id;
		}

		form['departmentId'] = this.form.controls.departmentId.value[0];
		form['saleDetails'] = this.getTableValues();
		
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];

			if (select.inputs) {
				for (const input of select.inputs) {
					form[input.name] = String(this.form.get(input.name).value);
				}
			}
		}

		for (const select of this.selectsSecond) {
			form[select['name']] = this.form.get(select['name']).value[0];

			if (select['inputs']) {
				for (const input of select['inputs']) {
					form[input.name] = this.form.get(input.name).value;
				}
			}
		}

		return form;
	}
	/**
	 * Get values from table.
	 *
	 * @private
	 * @returns {Array<object>}
	 * @memberof SaleComponent
	 */
	 private getTableValues(): Array<object> {
		 const values = [];

		 let index = 0;
		 for (const sale of this.form.controls.sales.value) {
			 const saleRequest = {
				'paidDate': sale[2].value,
				'paidImport': String(sale[3].value),
				'conceptId': this.selectsSecond[0]['options'].find(x => x.id === sale[4].id).id,
				'paymentMethodId': this.selectsSecond[1]['options'].find(x => x.id === sale[5].id).id,
				'statusId': this.selectsSecond[2]['options'].find(x => x.id === sale[6].id).id
			 }

			 values.push(saleRequest);
			 index++;
		 }

		 return values;
	 }
	/**
	 * Delete sale from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof SaleComponent
	 */
	public async deleteSale(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar la venta del departamento ' + this.sale.unitName + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Sales/DeleteSale', this.sale.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
	 * Enter here when clicked cancel button.
	 *
	 * @memberof SaleComponent
	 */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'sales' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
	/**
	 * Enter here when change any option in select component.
	 *
	 * @memberof SaleComponent
	 */
	 public selectChange(option): void {
		if (option.formControlName === "unit") {
			this.form.controls.salePrice.setValue(option.salePrice);
			this.form.controls.departmentType.setValue(option.type);
			this.form.controls.departmentId.setValue([option.departmentId]);
			this.form.controls.sales.setValue([]);
			this.form.controls.paid.setValue(0);
			this.form.controls.residue.setValue(0);
			this.form.controls.percentage.setValue(0);
		} else if (option.formControlName === "agentId") {
			this.form.controls.percentageAgent.setValue(option.percentaje);
		} else if (option.formControlName === "projectId") {
			this.selects[3]['options'] = option['departments'].map(x => ({id: x.unit, name: x.unit, type: x.type, departmentId: x.id, salePrice: x.salePrice}));
			this.form.controls.unit.setValue([option.departments[0].unit]);
			this.form.controls.salePrice.setValue(option.departments[0].salePrice);
			this.form.controls.departmentType.setValue(option.departments[0].type);
			this.form.controls.departmentId.setValue([option.departments[0].id]);
			this.form.controls.sales.setValue([]);
		}
		
		this.calculateTotalComisionAgent(this.form.controls.percentageAgent.value);
	}
	/**
	 * Enter here when type any value in input component.
	 *
	 * @memberof SaleComponent
	 */
	 public inputChange(inputName, value): void {
		if (inputName === 'salePrice') {
			this.calculateTotalComisionAgent(this.form.controls.percentageAgent.value);
		} else if (inputName === 'paid') {
			this.calculateTotalSale(value);
		} else if (inputName === 'percentageAgent') {
			this.calculateTotalComisionAgent(value);
		}
	}
	/**
	 * Enter here when type any value in input component.
	 *
	 * @memberof SaleComponent
	 */
	 public calculateTotalSale(paid): number {
		const salePrice = this.form.controls.salePrice.value;
		const total = salePrice - paid;
		
		if (total < 0) {
			Swal.fire({
				title: 'El saldo de la venta no puede ser negativo.',
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#00b800',
				confirmButtonText: 'Ok'
			});

			return null;
		}

		this.form.controls.residue.setValue(total);
		this.calculateTotalPercentage();

		return total;
	}
	/**
	 * Enter here when type any value in input component.
	 *
	 * @memberof SaleComponent
	 */
	public calculateTotalPercentage(): void {
		const totalPercentage = (this.form.controls.paid.value * 100) / this.form.controls.salePrice.value;
		this.form.controls.percentage.setValue(totalPercentage);
	}
	/**
	 * Enter here when type any value in input component.
	 *
	 * @memberof SaleComponent
	 */
	 public calculateTotalComisionAgent(percentage): void {
		const salePrice = this.form.controls.salePrice.value;
		const totalComision = (salePrice * percentage) / 100;
		this.form.controls.comisionAgent.setValue(totalComision);
	}
	/**
	 * Add sale.
	 *
	 * @memberof SaleComponent
	 */
	public async addSale(): Promise<any>
	{
		if (this.form.controls.paidImport.value < 0) {
			Swal.fire({
				title: 'El importe de la venta no puede ser negativo.',
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#00b800',
				confirmButtonText: 'Ok'
			});
			return;
		} else if (this.form.controls.unit.value === "") {
			Swal.fire({
				title: 'No se ha seleccionado ninguna unidad.',
				type: 'warning',
				showCancelButton: false,
				confirmButtonColor: '#00b800',
				confirmButtonText: 'Ok'
			});
			return;
		}

		const sales = this.form.controls.sales.value;
		
		if (this.editIndex !== null) {
			const sale = sales[this.editIndex];
			sale[5].value = this.getSelectName("paymentMethodId", this.form.controls.paymentMethodId.value);
			sale[5].id = this.getSelectId("paymentMethodId", this.form.controls.paymentMethodId.value);
			sale[6].value = this.getSelectName("statusId", this.form.controls.statusId.value);
			sale[6].id = this.getSelectId("statusId", this.form.controls.statusId.value);

			if (sale[6].value === 'Pagado') {
				let anotherPaids = 0;
				for (let index = 0; index < sales.length; index++) {
					if (index !== this.editIndex && sales[index][6].value === 'Pagado') {
						anotherPaids += parseFloat(sales[index][3].value);
					}
				}
				const totalPaid = anotherPaids + parseFloat(this.form.controls.paidImport.value);
				var total = await this.calculateTotalSale(totalPaid);
				
				if (total === null) {
					return;
				}

				this.form.controls.paid.setValue(totalPaid);
			} else {
				const totalPaid = parseFloat(this.form.controls.paid.value) - parseFloat(sale[3].value);
				var total = await this.calculateTotalSale(totalPaid);
				
				if (total === null) {
					return;
				}

				this.form.controls.paid.setValue(totalPaid);
			}

			sale[3].value = this.form.controls.paidImport.value;
			sale[2].value = this.form.controls.paidDate.value;
			sale[4].value = this.getSelectName("conceptId", this.form.controls.conceptId.value);
			sale[4].id = this.getSelectId("conceptId", this.form.controls.conceptId.value);
			this.editIndex = null;
		} else {
			var formsControlsNames = [
				{
					type: 'select',
					name: 'projectId'
				},
				{
					type: 'select',
					name: 'unit'
				},
				{
					type: 'input',
					name: 'paidDate'
				},
				{
					type: 'input',
					name: 'paidImport'
				},
				{
					type: 'select',
					name: 'conceptId'
				},
				{
					type: 'select',
					name: 'paymentMethodId'
				},
				{
					type: 'select',
					name: 'statusId'
				}
			];
			const sale = [];
			
			for (const { type, name } of formsControlsNames) {
				sale.push({
					name,
					value: (type === 'select') ? this.getSelectName(name, this.form.controls[name].value) : this.getInputName(name, this.form.controls[name].value),
					id: (type === 'select') ? this.getSelectId(name, this.form.controls[name].value) : null,
					typeOfMask: (type === 'input' && name == 'paidImport') ? 1 : null
				});
			}
			
			if (sale[6].value === 'Pagado') {
				const totalPaid = parseFloat(this.form.controls.paid.value) + parseFloat(sale[3].value);
				var total = await this.calculateTotalSale(totalPaid);
				
				if (total === null) {
					return;
				}

				this.form.controls.paid.setValue(totalPaid);
			}
	
			sales.push(sale);
		}

		sales.sort((left, right) => {
			return moment.utc(left[4].value).diff(moment.utc(right[4].value))
		});
		this.form.controls.sales.setValue(sales);
	}
	/**
	 * Get select name.
	 *
	 * @memberof SaleComponent
	 */
	public getSelectName(name, value): void
	{
		var allSelects = this.selects.concat(this.selectsSecond);
		var select = allSelects.find(x => x["name"] === name);
		var option = select['options'].find(x => x['id'] === value[0]);
		return option.name ? option.name : option.unit;
	}
	/**
	 * Get select name.
	 *
	 * @memberof SaleComponent
	 */
	 public getInputName(name, value): void
	 {
		var result = value;

		//if (name !== 'paidDate') {
		//	result = formatter.format(value);
		//}

		return result;
	 }
	/**
	 * Get select id.
	 *
	 * @memberof SaleComponent
	 */
	 public getSelectId(name, value): void
	 {
		 var allSelects = this.selects.concat(this.selectsSecond);
		 var select = allSelects.find(x => x["name"] === name);
		 var option = select['options'].find(x => x['id'] === value[0]);
 
		 return option.name ? option.id : option.unit;
	 }
	/**
	 * Get select id.
	 *
	 * @memberof SaleComponent
	 */
	public getSales (): Array<object> {
		if (this.sale) {
		const {
			sales
		} = this.sale;

		let salesResult = [];
		sales.forEach(sale => {
			// const company = this.selects[0]['options'].find(x => x.id === this.sale.companyId);

			const formsControlsNames = [
				{
					name: 'projectId',
					'value': this.selects[2]['options'].find(x => x.id === this.sale.projectId).name,
					'id': this.sale.projectId
				},
				{
					name: 'unit',
					'value': this.selects[3]['options'].find(x => x.departmentId === this.sale.departmentId).name,
					'id': null
				},
				{
					name: 'paidDate',
					'value': moment(sale.paidDate).format('YYYY-MM-DD'),
					'id': null
				},
				{
					name: 'paidImport',
					'value': sale.paidImport,
					'id': null,
					'typeOfMask': 1
				},
				{
					name: 'conceptId',
					'value': this.selectsSecond[0]['options'].find(x => x.id === sale.conceptId).name,
					'id': sale.conceptId
				},
				{
					name: 'paymentMethodId',
					'value': this.selectsSecond[1]['options'].find(x => x.id === sale.paymentMethodId).name,
					'id': sale.paymentMethodId
				},
				{
					name: 'statusId',
					'value': this.selectsSecond[2]['options'].find(x => x.id === sale.statusId).name,
					'id': sale.statusId
				}
			];
			const saleResult = [];
	
			for (const { name, value, id, typeOfMask } of formsControlsNames) {
				saleResult.push({
					name,
					value,
					id,
					typeOfMask
				});
			}
			salesResult.push(saleResult);
		});

		return salesResult;
		}
		
		return [];
	}
	/**
	 * Get select name.
	 *
	 * @memberof SaleComponent
	 */
	public editTableRow(index): void
	{
		const sale = this.form.controls.sales.value[index];
		this.form.controls.paidDate.setValue(sale[2].value);
		this.form.controls.paidImport.setValue(sale[3].value);
		this.form.controls.conceptId.setValue([sale[4].id]);
		this.form.controls.paymentMethodId.setValue([sale[5].id]);
		this.form.controls.statusId.setValue([sale[6].id]);
		this.editIndex = index;
	}
	/**
	 * Get select name.
	 *
	 * @memberof SaleComponent
	 */
	 public deleteTableRow(index): void
	 {
		 const sales = this.form.controls.sales.value;

		if (sales.length > 0) {
			sales.splice(index, 1);
			this.form.controls.sales.setValue(sales);
			let totalPaid = 0;
			sales.forEach(sale => {
				if (sale[6].value === 'Pagado') {
					totalPaid += parseInt(sale[3].value);
				}
			});

			this.form.controls.paid.setValue(totalPaid);
			this.calculateTotalSale(totalPaid);
		}
	}
}
