/*Angular imports*/
import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import moment from 'moment';
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { inputs, selects, secondSelects, table } from './template.json';

/**
 * The component of expense.
 *
 * @export
 * @class ExpenseComponent
 */
@Component({
	selector: 'app-expense',
	templateUrl: './expense.component.html',
	styleUrls: [ './expense.component.scss' ]
})
export class ExpenseComponent implements OnInit {
	/**
	 * Save expense object.
	 *
	 * @type {Array<object>}
	 * @memberof ExpenseComponent
	 */
	public expense: any;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof ExpenseComponent
	 */
	 public inputs: Array<object> = inputs;
	/**
	 * Selects template.
	 *
	 * @type {Array<object>}
	 * @memberof ExpenseComponent
	 */
	public selects: Array<object> = selects;
	/**
	 * Selects template.
	 *
	 * @type {Array<object>}
	 * @memberof ExpenseComponent
	 */
	public selectsSecond: Array<object> = secondSelects;
	/**
	 * Table template.
	 *
	 * @type {Array<object>}
	 * @memberof ExpenseComponent
	 */
	 public table: object = table;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof ExpenseComponent
	 */
	public form: FormGroup;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof ExpenseComponent
	 */
	public editIndex: number = null;
	/**
	 *Creates an instance of ExpenseComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ExpenseComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.expense = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.expense ? this.expense.id : 0),
			companyId: new FormControl(this.expense ? [this.expense.companyId] : '', [ Validators.required ]),
			officeId: new FormControl(this.expense ? [this.expense.officeId] : '', [ Validators.required ]),
			offices: new FormControl([]),
			projectId: new FormControl(this.expense ? [this.expense.projectId] : '', [ Validators.required ]),
			projects: new FormControl([]),
			typeId: new FormControl(this.expense ? [this.expense.typeId] : [1], [ Validators.required ]),
			expenseDate: new FormControl(this.expense ? moment(this.expense.expenseDate).format('YYYY-MM-DD') : moment().format('YYYY-MM-DD'), [ Validators.required ]),
			vendorId: new FormControl(this.expense ? [this.expense.vendorId] : '', [ Validators.required ]),
			vendors: new FormControl([]),
			conceptId: new FormControl(this.expense ? [this.expense.conceptId] : '', [ Validators.required ]),
			concepts: new FormControl([]),
			subConceptId: new FormControl(this.expense ? [this.expense.subConceptId] : '', [ Validators.required ]),
			contract: new FormControl(this.expense ? this.expense.contract : ''),
			advance: new FormControl(this.expense ? this.expense.advance : '', [Validators.maxLength(50) ]),
			advancePercentage: new FormControl(this.expense ? this.expense.advancePercentage : 0, [ Validators.maxLength(50) ]),
			imssPercentage: new FormControl(this.expense ? this.expense.imssPercentage : 0, [ Validators.maxLength(50) ]),
			factor: new FormControl(this.expense ? this.expense.factor : '', [ Validators.maxLength(30) ]),
			siroc: new FormControl(this.expense ? this.expense.siroc : '', [ Validators.maxLength(30) ]),
			paidDate: new FormControl(moment().format('YYYY-MM-DD')),
			expenseTypeId: new FormControl([ 2 ]),
			paidNumber: new FormControl('', [ Validators.maxLength(50) ]),
			paidImport: new FormControl('00.00', [ Validators.maxLength(50) ]),
			price: new FormControl('00.00', [ Validators.maxLength(50) ]),
			totalPrice: new FormControl('00.00', [ Validators.maxLength(50) ]),
			unit: new FormControl(''),
			counter: new FormControl(''),
			estimated: new FormControl(''),
			retention: new FormControl(''),
			amortization: new FormControl(''),
			expenses: new FormControl([])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof ExpenseComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();

		if (!this.expense) {
			this.setDisabledInputsType();
			this.setInputsTypeExpense();
		} else {
			this.selectChange('companyId', false);
			
			setTimeout(() => {
				this.selectChange('conceptId', false);
			}, 100);
			this.form.controls.typeId.setValue([this.expense.typeId]);

			if (this.expense.expenses.length > 0) {
				this.form.controls.expenseTypeId.setValue([this.expense.expenses[0].expenseTypeId]);
				this.selectChange('expenseTypeId', false);
			}

			this.selectChange('typeId', false);

			setTimeout(() => {
				this.form.controls.expenses.setValue(this.getExpenses());
			}, 100);
		}
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies.data;
			const companyId = this.expense ? this.expense.companyId : companies.data[0].id;
			this.form.controls.companyId.setValue([companyId]);
			this.searchOffices(companyId);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	 public async searchOffices(companyId): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.form.controls.offices.setValue(offices.data);
			this.selects[1]['options'] = offices.data.filter(x => x.companyId === companyId);
			const officeId = this.expense ? this.expense.officeId : this.selects[1]['options'][0].id;
			this.form.controls.officeId.setValue([officeId]);
			this.searchProjects(companyId, officeId);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get projects.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	 public async searchProjects(companyId, officeId): Promise<any> {
		try {
			const projects = await this.apiProvider.doGet('Projects/GetProjects/false');
			this.form.controls.projects.setValue(projects.data);
			this.selects[2]['options'] = projects['data'].filter(x => x.companyId === companyId && x.officeId === officeId);
			this.form.controls.projectId.setValue(this.expense ? [this.expense.projectId] : [this.selects[2]['options'][0].id]);
			this.searchVendors(companyId, officeId);
			this.searchConcepts(companyId, officeId);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get vendors.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	 public async searchVendors(companyId, officeId): Promise<any> {
		try {
			const vendors = await this.apiProvider.doGet('Vendors/GetVendors');
			this.form.controls.vendors.setValue(vendors.data);
			this.selects[4]['options'] = vendors.data.filter(x => x.companyId === companyId && x.officeId === officeId);
			this.form.controls.vendorId.setValue(this.expense ? [this.expense.vendorId] : [this.selects[4]['options'][0].id]);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get concepts.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	 public async searchConcepts(companyId, officeId): Promise<any> {
		try {
			const concepts = await this.apiProvider.doGet('Concepts/GetConcepts');
			this.form.controls.concepts.setValue(concepts.data);
			this.selects[5]['options'] = concepts.data.filter(x => x.companyId === companyId && x.officeId === officeId);
			this.form.controls.conceptId.setValue(this.expense ? [this.expense.conceptId] : [this.selects[5]['options'][0].id]);

			if (this.expense) {
				const concepts = this.selects[5]['options'].find(x => x.id === this.expense.conceptId);
				this.selects[6]['options'] = concepts.subConcepts;
				this.form.controls.subConceptId.setValue([this.expense.subConceptId]);
			} else if (this.selects[5]['options'][0].subConcepts.length > 0) {
				this.selects[6]['options'] = this.selects[5]['options'][0].subConcepts;
				this.form.controls.subConceptId.setValue([this.selects[6]['options'][0].id]);
			} else {
				this.selects[6]['options'] = [];
				this.form.controls.subConceptId.setValue([]);
			}

			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter here when change any option in select component.
	 *
	 * @memberof ExpenseComponent
	 */
	public selectChange(option, clearInputs = true): void {
		if (option === 'companyId') {
			this.selects[1]['options'] = this.form.controls.offices.value.filter(x => x.companyId === this.form.controls.companyId.value[0]);

			if (clearInputs) {
				this.form.controls.officeId.setValue([this.selects[1]['options'][0]['id']]);
			}

			this.selectChange('officeId');
		} else if (option === 'officeId') {
			this.selects[2]['options'] = this.form.controls.projects.value.filter(x => x.companyId === this.form.controls.companyId.value[0] && x.officeId === this.form.controls.officeId.value[0]);
			
			if (this.selects[2]['options'].length > 0) {
				this.form.controls.projectId.setValue([this.selects[2]['options'][0]['id']]);
			}
			this.selects[4]['options'] = this.form.controls.vendors.value.filter(x => x.companyId === this.form.controls.companyId.value[0] && x.officeId === this.form.controls.officeId.value[0]);
			
			if (this.selects[4]['options'].length > 0) {
				this.form.controls.vendorId.setValue([this.selects[4]['options'][0]['id']]);
			}

			this.selects[5]['options'] = this.form.controls.concepts.value.filter(x => x.companyId === this.form.controls.companyId.value[0] && x.officeId === this.form.controls.officeId.value[0]);
			this.selects[6]['options'] = [];

			if (this.selects[5]['options'].length > 0) {
				this.form.controls.conceptId.setValue([this.selects[5]['options'][0]['id']]);
	
				if (this.selects[5]['options'][0]['subConcepts'].length > 0) {
					this.selects[6]['options'] = this.selects[5]['options'][0]['id']['subConcepts'];
				}
			}
		} else if (option === 'conceptId') {
			const concept = this.form.controls.concepts.value.find(x => x.id == this.form.controls.conceptId.value);
			if (concept && concept.subConcepts.length > 0) {
				this.selects[6]['options'] = concept.subConcepts;
				this.form.controls.subConceptId.setValue([this.selects[6]['options'][0].id]);
			} else {
				this.selects[6]['options'] = [];
				this.form.controls.subConceptId.setValue([]);
			}
		} else if (option === 'typeId') {
			if (clearInputs) {
				this.clearContractInputs();
			}
			if (this.form.controls.typeId.value[0] === 0) {
				this.setEnabledInputsType();
				this.selectsSecond[0]['options'] = [
					{
						"name": "Estimación",
						"id": 1
					},
					{
						"name": "Anticipo",
						"id": 0
					}
				];

				//if (this.form.controls.expenseTypeId.value[0] === 0) {
				//	this.setInputsTypeAdvance();
				//	this.form.controls.expenseTypeId.setValue([0]);
				//	this.table["columns"] = [ "Fecha", "Tipo egreso", "No pago", "Total a pagar"];
				//} else {
				this.setInputsTypeEstimation();
				this.form.controls.expenseTypeId.setValue([1]);
				this.table["columns"] = [ "Fecha", "Tipo egreso", "No pago", "Total a pagar", "Contador", "Estimado", "Retención", "Amortización"];
				//}
			} else {
				this.setDisabledInputsType();
				this.selectsSecond[0]['options'] = [
					{
					  "name": "Egresos",
					  "id": 2
					}
				  ];
				this.form.controls.expenseTypeId.setValue([2]);
				this.table["columns"] = [ "Fecha", "Tipo egreso", "No pago", "Cantidad", "Precio", "Unidad medida", "Total a pagar"];
			}
		} else if (option === 'expenseTypeId') {
			if (clearInputs) {
				this.clearExpenseType();
			}
			
			//if (this.form.controls.expenseTypeId.value[0] === 0) {
			//	this.setInputsTypeAdvance();
			//} else {
			this.setInputsTypeEstimation();
			//}
		}
	}
	/**	
	 * Clear contract inputs.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	private clearContractInputs(): void {
		this.form.controls.expenses.setValue([]);
		this.form.controls.contract.setValue('');
		this.form.controls.advance.setValue('');
		this.form.controls.advancePercentage.setValue(0);
		this.form.controls.imssPercentage.setValue(0);
		this.form.controls.factor.setValue('');
		this.form.controls.siroc.setValue('');
		this.clearExpenseType();
	}
	/**	
	 * Clear contract inputs.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	 private clearExpenseType(): void {
		this.form.controls.expenses.setValue([]);
		this.form.controls.paidNumber.setValue('');
		this.form.controls.paidImport.setValue('00.00');
		this.form.controls.price.setValue('00.00');
		this.form.controls.totalPrice.setValue('00.00');
		this.form.controls.unit.setValue('');
		this.form.controls.counter.setValue('');
		this.form.controls.estimated.setValue('');
		this.form.controls.retention.setValue('');
		this.form.controls.amortization.setValue('');
	}
	/**
	 * Set disabled inputs type.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	private setDisabledInputsType(): void {
		this.selects[6]['inputs'][0]['class'] = "disabled";
		this.selects[6]['inputs'][1]['class'] = "disabled";
		this.selects[6]['inputs'][2]['class'] = "disabled";
		this.selects[6]['inputs'][3]['class'] = "disabled";
		this.selects[6]['inputs'][4]['class'] = "disabled";
		this.selects[6]['inputs'][5]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][0]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][1]['class'] = "";
		this.selectsSecond[0]['inputs'][2]['class'] = "";
		this.selectsSecond[0]['inputs'][3]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][4]['class'] = "";
		this.selectsSecond[0]['inputs'][5]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][6]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][7]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][8]['class'] = "disabled";
		this.selectsSecond[0]['options'] = [
			{
			  "name": "Egresos",
			  "id": 2
			}
		  ];
	}
	/**
	 * Set enabled inputs type.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	private setEnabledInputsType(): void {
		this.selects[6]['inputs'][0]['class'] = "";
		this.selects[6]['inputs'][1]['class'] = "";
		this.selects[6]['inputs'][2]['class'] = "";
		this.selects[6]['inputs'][3]['class'] = "";
		this.selects[6]['inputs'][4]['class'] = "";
		this.selects[6]['inputs'][5]['class'] = "";
		this.selectsSecond[0]['inputs'][0]['class'] = "";
		this.selectsSecond[0]['inputs'][1]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][2]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][3]['class'] = "";
		this.selectsSecond[0]['inputs'][4]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][5]['class'] = "";
		this.selectsSecond[0]['inputs'][6]['class'] = "";
		this.selectsSecond[0]['inputs'][7]['class'] = "";
		this.selectsSecond[0]['inputs'][8]['class'] = "";
	}
	/**
	 * Set disabled inputs type expense.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	 private setInputsTypeExpense(): void {
		this.selectsSecond[0]['inputs'][5]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][6]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][7]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][8]['class'] = "disabled";
		this.table["columns"] = [ "Fecha", "Tipo egreso", "No pago", "Cantidad", "Precio", "Unidad medida", "Total a pagar"];
	}
	/**
	 * Set enabled inputs type expense.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	private setInputsTypeEstimation(): void {
		this.selectsSecond[0]['inputs'][5]['class'] = "";
		// this.selectsSecond[0]['inputs'][6]['class'] = "";
		// this.selectsSecond[0]['inputs'][7]['class'] = "";
		// this.selectsSecond[0]['inputs'][8]['class'] = "";
		this.table["columns"] = [ "Fecha", "Tipo egreso", "No pago", "Total a pagar", "Contador", "Estimado", "Retención", "Amortización"];
	}
	/**
	 * Set disabled inputs type advance.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	private setInputsTypeAdvance(): void {
		this.selectsSecond[0]['inputs'][5]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][6]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][7]['class'] = "disabled";
		this.selectsSecond[0]['inputs'][8]['class'] = "disabled";
		this.table["columns"] = [ "Fecha", "Tipo egreso", "No pago", "Total a pagar"];
	}
	/**
	 * Add sale in database.
	 *
	 * @memberof ExpenseComponent
	 */
	public async saveExpense(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Expenses/UpdateExpense', form);
			} else {
				response = await this.apiProvider.doPost('Expenses/SaveExpense', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof ExpenseComponent
	 */
	private getValues(): object {
		const form = {};

		if (this.expense) {
			form['id'] = this.expense.id;
		}

		form['expenseDetails'] = this.getTableValues();
		
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];

			if (select.inputs) {
				for (const input of select.inputs) {
					form[input.name] = String(this.form.get(input.name).value);
				}
			}
		}

		for (const select of this.selectsSecond) {
			form[select['name']] = this.form.get(select['name']).value[0];

			if (select['inputs']) {
				for (const input of select['inputs']) {
					form[input.name] = this.form.get(input.name).value;
				}
			}
		}

		return form;
	}
	/**
	 * Get values from table.
	 *
	 * @private
	 * @returns {Array<object>}
	 * @memberof ExpenseComponent
	 */
	private getTableValues(): Array<object> {
		const values = [];

		let index = 0;
		for (const sale of this.form.controls.expenses.value) {
			const saleRequest: any = {
			'paidDate': sale[0].value,
			'expenseTypeId': this.selectsSecond[0]['options'].find(x => x.id === sale[1].id).id,
			'paidNumber': sale[2].value
			}

			if (this.form.controls.expenseTypeId.value[0] === 2) {
				saleRequest.paidImport = String(sale[3].value);
				saleRequest.price = String(sale[4].value);
				saleRequest.unit = sale[5].value;
				saleRequest.totalPrice = String(sale[6].value);
				saleRequest.counter = '';
				saleRequest.estimated = '00.00';
				saleRequest.retention = '00.00';
				saleRequest.amortization = '00.00';
			} else { //if (this.form.controls.expenseTypeId.value[0] === 1) {
				saleRequest.paidImport = '00.00';
				saleRequest.price = '00.00';
				saleRequest.unit = '';
				saleRequest.totalPrice = String(sale[3].value);
				saleRequest.counter = sale[4].value;
				saleRequest.estimated = String(sale[5].value);
				saleRequest.retention = String(sale[6].value);
				saleRequest.amortization = String(sale[7].value);
			} /*else {
				saleRequest.paidImport = '00.00';
				saleRequest.price = '00.00';
				saleRequest.unit = '';
				saleRequest.totalPrice = String(sale[3].value);
				saleRequest.counter = '';
				saleRequest.estimated = '00.00';
				saleRequest.retention = '00.00';
				saleRequest.amortization = '00.00';
			}*/

			values.push(saleRequest);
			index++;
		}

		return values;
	}
	/**
	 * Enter here when type any value in input component.
	 *
	 * @memberof ExpenseComponent
	 */
	 public inputChange(inputName, value): void {
		if (inputName === 'paidImport' || inputName === 'price') {
			this.calculateTotal();
		} else if (inputName === 'totalPrice' || inputName === 'factor') {
			if (this.form.controls.expenseTypeId.value[0] === 1) {	
				this.calculateEstimated();
			}
		} else if (inputName === 'estimated' || inputName === 'imssPercentage') {
			if (this.form.controls.expenseTypeId.value[0] === 1) {	
				this.calculateRetention();
			}
		} else if (inputName === 'estimated' || inputName === 'advancePercentage') {
			if (this.form.controls.expenseTypeId.value[0] === 1) {	
				this.calculateAmortization();
			}
		}
	}
	/**
	 * Calculate total paid import * price.
	 *
	 * @memberof ExpenseComponent
	 */
	public calculateTotal(): void {
		const total = parseFloat(this.form.controls.paidImport.value) * parseFloat(this.form.controls.price.value);
		this.form.controls.totalPrice.setValue(total);
	}
	/**
	 * Calculate estimated total price * factor.
	 *
	 * @memberof ExpenseComponent
	 */
	public calculateEstimated(): void {
		const estimated = parseFloat(this.form.controls.totalPrice.value) * parseFloat(this.form.controls.factor.value);
		this.form.controls.estimated.setValue(estimated);
		this.calculateRetention();
		this.calculateAmortization();
	}
	/**
	 * Calculate retention estimated * imssPercentage.
	 *
	 * @memberof ExpenseComponent
	 */
	public calculateRetention(): void {
		const retention = parseFloat(this.form.controls.estimated.value) * parseFloat(this.form.controls.imssPercentage.value);
		this.form.controls.retention.setValue(retention);
		this.calculateAmortization();
	}
	/**
	 * Calculate amortization total price * factor.
	 *
	 * @memberof ExpenseComponent
	 */
	public calculateAmortization(): void {
		const amortization = (parseFloat(this.form.controls.estimated.value) * parseFloat(this.form.controls.advancePercentage.value)) / 100;
		this.form.controls.amortization.setValue(amortization);
	}
	/**
	 * Add expense.
	 *
	 * @memberof ExpenseComponent
	 */
	public addExpense(): void
	{
		const expenses = this.form.controls.expenses.value;
		
		if (this.editIndex !== null) {
			const expense = expenses[this.editIndex];
			expense[0].value = this.form.controls.paidDate.value;
			expense[1].id =  this.form.controls.expenseTypeId.value[0];
			//expense[1].value = this.form.controls.expenseTypeId.value[0].value;

			if (this.form.controls.expenseTypeId.value[0] === 2) {
				expense[2].value = this.form.controls.paidNumber.value;
				expense[3].value = this.form.controls.paidImport.value;
				expense[4].value = this.form.controls.price.value;
				expense[5].value = this.form.controls.unit.value;
				expense[6].value = this.form.controls.totalPrice.value;
			} else { // if (this.form.controls.expenseTypeId.value[0] === 1) {
				expense[2].value = this.form.controls.paidNumber.value;
				expense[3].value = this.form.controls.totalPrice.value;
				expense[4].value = this.form.controls.counter.value;
				expense[5].value = this.form.controls.estimated.value;
				expense[6].value = this.form.controls.retention.value;
				expense[7].value = this.form.controls.amortization.value;
			// } else {
				// expense[2].value = this.form.controls.paidNumber.value;
				// expense[3].value = this.form.controls.totalPrice.value;
			}

			this.form.controls.expenses.setValue(expenses);
			this.editIndex = null;
		} else {
			let formsControlsNames = this.getFormControlNames();
			const expense = [];
			
			for (const { type, name } of formsControlsNames) {
				expense.push({
					name,
					value: (type === 'select') ? this.getSelectName(name, this.form.controls[name].value) : this.getInputName(name, this.form.controls[name].value),
					id: (type === 'select') ? this.getSelectId(name, this.form.controls[name].value) : null,
					typeOfMask: (type === 'input' && name == 'paidImport') || (type === 'input' && name == 'price')
						|| (type === 'input' && name == 'totalPrice') || (type === 'input' && name == 'estimated')
						|| (type === 'input' && name == 'retention') || (type === 'input' && name == 'amortization') ? 1 : null
				});
			}
	
			expenses.push(expense);
		}

		this.form.controls.expenses.setValue(expenses);
	}
	/**
	 * Get form control names.
	 *
	 * @memberof ExpenseComponent
	 */
	public getFormControlNames(): Array<any> {
		let formsControlsNames = [{}];

		if (this.form.controls.expenseTypeId.value[0] === 2) {
			formsControlsNames = [{
				type: 'input',
				name: 'paidDate'
			},
			{
				type: 'select',
				name: 'expenseTypeId'
			},
			{
				type: 'input',
				name: 'paidNumber'
			},
			{
				type: 'input',
				name: 'paidImport'
			},
			{
				type: 'input',
				name: 'price'
			},
			{
				type: 'input',
				name: 'unit'
			},
			{
				type: 'input',
				name: 'totalPrice'
			}];
		} else {  //if (this.form.controls.expenseTypeId.value[0] === 1) {
			formsControlsNames = [{
				type: 'input',
				name: 'paidDate'
			},
			{
				type: 'select',
				name: 'expenseTypeId'
			},
			{
				type: 'input',
				name: 'paidNumber'
			},
			{
				type: 'input',
				name: 'totalPrice'
			},
			{
				type: 'input',
				name: 'counter'
			},
			{
				type: 'input',
				name: 'estimated'
			},
			{
				type: 'input',
				name: 'retention'
			},
			{
				type: 'input',
				name: 'amortization'
			}];
		} /*else {
			formsControlsNames = [{
				type: 'input',
				name: 'paidDate'
			},
			{
				type: 'select',
				name: 'expenseTypeId'
			},
			{
				type: 'input',
				name: 'paidNumber'
			},
			{
				type: 'input',
				name: 'totalPrice'
			}];
		}*/

		return formsControlsNames;
	}
	/**
	 * Get select id.
	 *
	 * @memberof SaleComponent
	 */
	 public getExpenses (): Array<object> {
		if (this.expense) {
		const {
			expenses
		} = this.expense;

		let expensesResult = [];
		expenses.forEach(expense => {
			let formsControlsNames: any = [{
				name: 'paidDate',
				value: moment(expense.paidDate).format('YYYY-MM-DD'),
				id: null
			},
			{
				name: 'expenseTypeId',
				value: this.selectsSecond[0]['options'].find(x => x.id === expense.expenseTypeId).name,
				id: expense.expenseTypeId
			},
			{
				name: 'paidNumber',
				value: expense.paidNumber,
				id: null
			}];

			if (expense.expenseTypeId === 2) {
				formsControlsNames = [...formsControlsNames, ...[{
					name: 'paidImport',
					value: expense.paidImport,
					id: null,
					typeOfMask: 1
				},
				{
					name: 'price',
					value: expense.price,
					id: null,
					typeOfMask: 1
				},
				{
					name: 'unit',
					value: expense.unit,
					id: null
				},
				{
					name: 'totalPrice',
					value: expense.totalPrice,
					id: null,
					typeOfMask: 1
				}]];
			} else { //if (expense.expenseTypeId === 1) {
				formsControlsNames = [...formsControlsNames, ...[{
					name: 'totalPrice',
					value: expense.totalPrice,
					id: null,
					typeOfMask: 1
				},
				{
					name: 'counter',
					value: expense.counter,
					id: null
				},
				{
					name: 'estimated',
					value: expense.estimated,
					id: null,
					typeOfMask: 1
				},
				{
					name: 'retention',
					value: expense.retention,
					id: null,
					typeOfMask: 1
				},
				{
					name: 'amortization',
					value: expense.amortization,
					id: null,
					typeOfMask: 1
				}]];
			} /*else {
				formsControlsNames = [...formsControlsNames, ...[{
					name: 'totalPrice',
					value: expense.totalPrice,
					id: null,
					typeOfMask: 1
				}]];
			}*/

			const expenseResult = [];
	
			for (const { name, value, id, typeOfMask } of formsControlsNames) {
				expenseResult.push({
					name,
					value,
					id,
					typeOfMask
				});
			}
			expensesResult.push(expenseResult);
		});

		return expensesResult;
		}
		
		return [];
	}
	/**
	 * Get select name.
	 *
	 * @memberof ExpenseComponent
	 */
	public getSelectName(name, value): void
	{
		var allSelects = this.selects.concat(this.selectsSecond);
		var select = allSelects.find(x => x["name"] === name);
		var option = select['options'].find(x => x['id'] === value[0]);
		return option.name ? option.name : option.unit;
	}
	/**
	 * Get select name.
	 *
	 * @memberof ExpenseComponent
	 */
	public getInputName(name, value): void
	{
		var result = value;

		//if (name !== 'paidDate') {
		//	result = formatter.format(value);
		//}

		return result;
	}
	/**
	 * Get select id.
	 *
	 * @memberof ExpenseComponent
	 */
	public getSelectId(name, value): void
	{
		var allSelects = this.selects.concat(this.selectsSecond);
		var select = allSelects.find(x => x["name"] === name);
		var option = select['options'].find(x => x['id'] === value[0]);

		return option.name ? option.id : option.unit;
	}
	/**
	 * Delete expense from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpenseComponent
	 */
	public async deleteExpense(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar el egreso del proveedor ' + this.expense.vendorName + '?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Expenses/DeleteExpense', this.expense.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
	 * Enter here when clicked cancel button.
	 *
	 * @memberof ExpenseComponent
	 */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'expenses' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
	/**
	 * Edit table row select name.
	 *
	 * @memberof SaleComponent
	 */
	public editTableRow(index): void
	{
		const expense = this.form.controls.expenses.value[index];
		this.form.controls.paidDate.setValue(expense[0].value);
		this.form.controls.expenseTypeId.setValue([expense[1].id]);

		if (this.form.controls.expenseTypeId.value[0] === 2) {
			this.form.controls.paidNumber.setValue(expense[2].value);
			this.form.controls.paidImport.setValue(expense[3].value);
			this.form.controls.price.setValue(expense[4].value);
			this.form.controls.unit.setValue(expense[5].value);
			this.form.controls.totalPrice.setValue(expense[6].value);
		} else { // if (this.form.controls.expenseTypeId.value[0] === 1) {
			this.form.controls.paidNumber.setValue(expense[2].value);
			this.form.controls.totalPrice.setValue(expense[3].value);
			this.form.controls.counter.setValue(expense[4].value);
			this.form.controls.estimated.setValue(expense[5].value);
			this.form.controls.retention.setValue(expense[6].value);
			this.form.controls.amortization.setValue(expense[7].value);
		} /*else {
			this.form.controls.paidNumber.setValue(expense[2].value);
			this.form.controls.totalPrice.setValue(expense[3].value);
		}*/
		this.editIndex = index;
	}
	/**
	 * Delete table row.
	 *
	 * @memberof ExpenseComponent
	 */
	public deleteTableRow(index): void
	{
		 const expenses = this.form.controls.expenses.value;

		if (expenses.length > 0) {
			expenses.splice(index, 1);
			this.form.controls.expenses.setValue(expenses);
		}
	}
}
