/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of sales.
 *
 * @export
 * @class SalesComponent
 */
@Component({
	selector: 'app-sales',
	templateUrl: './sales.component.html',
	styleUrls: [ './sales.component.scss' ]
})
export class SalesComponent {
	/**
	 * Local reference to array of sales.
	 *
	 * @type {Array<object>}
	 * @memberof SalesComponent
	 */
	public sales: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof SalesComponent
	 */
	public originalSales: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof SalesComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof SalesComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of SalesComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof SalesComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof SalesComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.sales = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof SalesComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const sales = await this.apiProvider.doGet('Sales/GetSales');
			this.sales = sales['data'];
			this.originalSales = sales['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof UsersComponent
	 */
	 public search(text): void {
		if (text === "") {
			this.sales = this.originalSales;
		} else if (this.originalSales.length > 0) {
			this.sales = this.originalSales.filter(x => x["unitName"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof SalesComponent
	 */
	public addSale(sale = null): void {
		this.router.navigate([ { outlets: { app: 'sale' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: sale
			}
		});
	}
}
