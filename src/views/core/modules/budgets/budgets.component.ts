/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of budgets.
 *
 * @export
 * @class BudgetsComponent
 */
@Component({
	selector: 'app-budgets',
	templateUrl: './budgets.component.html',
	styleUrls: [ './budgets.component.scss' ]
})
export class BudgetsComponent {
	/**
	 * Local reference to array of budgets.
	 *
	 * @type {Array<object>}
	 * @memberof BudgetsComponent
	 */
	public budgets: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof BudgetsComponent
	 */
	public originalBudgets: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof BudgetsComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof BudgetsComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of BudgetsComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof BudgetsComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof BudgetsComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.budgets = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetsComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const budgets = await this.apiProvider.doGet('Budgets/GetBudgets');
			this.budgets = budgets['data'];
			this.originalBudgets = budgets['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof BudgetsComponent
	 */
	public search(text): void {
		if (text === "") {
			this.budgets = this.originalBudgets;
		} else if (this.originalBudgets.length > 0) {
			this.budgets = this.originalBudgets.filter(x => x["projectName"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof BudgetsComponent
	 */
	public addBudget(budget = null): void {
		this.router.navigate([ { outlets: { app: 'budget' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: budget
			}
		});
	}
}
