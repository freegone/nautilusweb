/*Angular imports*/
import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { selects, secondSelects, table } from './template.json';

/**
 * The component of budget.
 *
 * @export
 * @class BudgetComponent
 */
@Component({
	selector: 'app-budget',
	templateUrl: './budget.component.html',
	styleUrls: [ './budget.component.scss' ]
})
export class BudgetComponent implements OnInit {
	/**
	 * Save budget object.
	 *
	 * @type {Array<object>}
	 * @memberof BudgetComponent
	 */
	public budget: any;
	/**
	 * Selects template.
	 *
	 * @type {Array<object>}
	 * @memberof BudgetComponent
	 */
	public selects: Array<object> = selects;
	/**
	 * Selects template.
	 *
	 * @type {Array<object>}
	 * @memberof BudgetComponent
	 */
	public selectsSecond: Array<object> = secondSelects;
	/**
	 * Table template.
	 *
	 * @type {Array<object>}
	 * @memberof BudgetComponent
	 */
	 public table: object = table;
	/**
	 * Form template.
	 *
	 * @type {FormGroup}
	 * @memberof BudgetComponent
	 */
	public form: FormGroup;
	/**
	 * Edit index.
	 *
	 * @type {number}
	 * @memberof BudgetComponent
	 */
	public editIndex: number = null;
	/**
	 *Creates an instance of BudgetComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof BudgetComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.budget = this.router.getCurrentNavigation().extras.state.data;
		this.form = new FormGroup({
			id: new FormControl(this.budget ? this.budget.id : 0),
			companyId: new FormControl(this.budget ? this.budget.companyId : '', [ Validators.required ]),
			officeId: new FormControl(this.budget ? this.budget.officeId : '', [ Validators.required ]),
			offices: new FormControl([]),
			projectId: new FormControl(this.budget ? this.budget.projectId : '', [ Validators.required ]),
			projects: new FormControl([]),
			programs: new FormControl([]),
			concepts: new FormControl([]),
			sales: new FormControl([]),
			expenses: new FormControl([]),
			typeId: new FormControl(this.budget ? this.budget.typeId : [1], [ Validators.required ]),
			projection: new FormControl(this.budget ? this.budget.projection : '0', [ Validators.required ]),
			executed: new FormControl(this.budget ? this.budget.executed : '0', [ Validators.required ]),
			advance: new FormControl(this.budget ? this.budget.advance : '0', [ Validators.required ]),			
			yearId: new FormControl([0]),
			monthId: new FormControl([0]),
			weekId: new FormControl([0]),
			programId: new FormControl(''),
			conceptId: new FormControl(''),
			total: new FormControl('00.00'),
			budgets: new FormControl([])
		});
	}
	/**
	 * Execute when enter to user.
	 *
	 * @memberof BudgetComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.searchCompanies();
		this.searchVendors();
		this.searchConcepts();
		this.searchPrograms().then(() => {

			if (!this.budget) {
				this.setInputsType();
			} else {
				this.form.controls.typeId.setValue([this.budget.typeId]);

				setTimeout(() => {
					this.selectChange('typeId');
				}, 100);

				setTimeout(() => {
					this.form.controls.budgets.setValue(this.getBudgets());
				}, 100);
			}
			
			this.myapp.setLoading(false);
		});
	}
	/**
	 * Get companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	public async searchCompanies(): Promise<any> {
		try {
			const companies = await this.apiProvider.doGet('Companies/GetCompanies');
			this.selects[0]['options'] = companies['data'];
			const companyId = this.budget ? this.budget.companyId : this.selects[0]['options'][0]['id'];
			this.form.controls.companyId.patchValue([companyId]);
			this.searchOffices(companyId);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get offices.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchOffices(companyId): Promise<any> {
		try {
			const offices = await this.apiProvider.doGet('Offices/GetOffices');
			this.form.controls.offices.patchValue(offices['data']);
			this.selects[1]['options'] = offices['data'].filter(x => x.companyId === companyId);
			const officeId = this.budget ? this.budget.officeId : this.selects[1]['options'][0]['id'];
			this.form.controls.officeId.patchValue([officeId]);
			this.searchProjects(companyId, officeId);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get projects.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchProjects(companyId, officeId): Promise<any> {
		try {
			const projects = await this.apiProvider.doGet('Projects/GetProjects/false');
			this.form.controls.projects.patchValue(projects['data']);
			this.selects[2]['options'] = projects['data'].filter(x => x.companyId === companyId && x.officeId === officeId);
			const projectId = this.budget ? this.budget.projectId : this.selects[2]['options'][0]['id'];
			this.form.controls.projectId.patchValue([projectId]);
			this.searchSales();
			this.searchExpenses();
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get vendors.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchVendors(): Promise<any> {
		try {
			const vendors = await this.apiProvider.doGet('Vendors/GetVendors');
			this.selects[4]['options'] = vendors['data'];
			this.form.controls.vendorId.patchValue(this.budget ? [this.budget.vendorId] : [this.selects[4]['options'][0]['id']]);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get departments.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchDepartments(): Promise<any> {
		try {
			const departments = await this.apiProvider.doGet('Departments/GetDepartments');
			departments['data'] = departments['data'].map(x => ({ ...x, name: x.type }));
			//this.selects[4]['options'] = departments['data'];
			this.form.controls.departmentType.setValue(this.budget ? [this.budget.departmentType] : [this.selects[4]['options'][0]['id']]);
			this.form.controls.salePrice.setValue(this.budget ? this.budget.salePrice : [this.selects[4]['options'][0]['salePrice']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get customers.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchCustomers(): Promise<any> {
		try {
			const customers = await this.apiProvider.doGet('Customers/GetCustomers');
			//this.selects[4]['options'] = customers['data'];
			this.form.controls.customerId.setValue(this.budget ? [this.budget.customerId] : [this.selects[4]['options'][0]['id']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get agents.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchAgents(): Promise<any> {
		try {
			const agents = await this.apiProvider.doGet('Agents/GetAgents');
			this.selects[5]['options'] = agents['data'];
			this.form.controls.agentId.setValue(this.budget ? [this.budget.agentId] : [this.selects[5]['options'][0]['id']]);
			this.form.controls.percentageAgent.setValue(this.budget ? this.budget.percentageAgent : [this.selects[5]['options'][0]['percentaje']]);
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get concepts.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchConcepts(): Promise<any> {
		try {
			const concepts = await this.apiProvider.doGet('Concepts/GetConcepts');
			this.form.controls.concepts.patchValue(concepts['data']);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get programs.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	public async searchPrograms(): Promise<any> {
		try {
			const programs = await this.apiProvider.doGet('Programs/GetPrograms');
			this.form.controls.programs.patchValue(programs['data']);
			this.selectsSecond[3]['options'] = programs['data'];
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get sales.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchExpenses(): Promise<any> {
		try {
			const expenses = await this.apiProvider.doGet('Expenses/GetExpenses');
			this.form.controls.expenses.patchValue(expenses['data']);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get sales.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	 public async searchSales(): Promise<any> {
		try {
			const sales = await this.apiProvider.doGet('Sales/GetSales');
			this.form.controls.sales.patchValue(sales['data']);
			this.setExecuted();
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter here when change any option in select component.
	 *
	 * @memberof BudgetComponent
	 */
	public selectChange(option): void {
		if (option === 'companyId') {
			this.selects[1]['options'] = this.form.controls.offices.value.filter(x => x.companyId === this.form.controls.companyId.value[0]);
			this.form.controls.officeId.setValue([this.selects[1]['options'][0]['id']]);
			this.selects[2]['options'] = this.form.controls.projects.value.filter(x => x.companyId === this.form.controls.companyId.value[0] && x.officeId === this.form.controls.officeId.value[0]);
			this.selectChange('officeId');
		} else if (option === 'officeId') {
			this.selects[2]['options'] = this.form.controls.projects.value.filter(x => x.companyId === this.form.controls.companyId.value[0] && x.officeId === this.form.controls.officeId.value[0]);
			this.form.controls.budgets.setValue([]);
			
			if (this.selects[2]['options'].length > 0) {
				this.form.controls.projectId.setValue([this.selects[2]['options'][0]['id']]);
			}
		} else if (option === 'typeId') {
			this.setInputsType();
		}
	}
	/**
	 * Set disabled inputs type expense.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	private setInputsType(): void {
		// this.budget = null;
		// this.form.controls.budgets.setValue([]);

		if (this.form.controls.typeId.value[0] === 1) {
			this.selectsSecond[3]['label'] = 'Programa';
			this.selectsSecond[3]['options'] = this.form.controls.programs.value;
			this.selectsSecond[3]['name'] = 'programId';
			this.table['columns'] =  ["Empresa", "Sucursal", "Proyecto", "Tipo", "Año", "Mes", "Semana", "Programa", "Total"];
			this.form.controls.programId.patchValue([this.selectsSecond[3]['options'][0]['id']]);
			this.setExecuted();
		} else {
			this.selectsSecond[3]['label'] = 'Concepto';
			this.selectsSecond[3]['options'] = this.form.controls.concepts.value;
			this.selectsSecond[3]['name'] = 'conceptId';
			this.table['columns'] =  ["Empresa", "Sucursal", "Proyecto", "Tipo", "Año", "Mes", "Semana", "Concepto", "Total"];
			this.form.controls.conceptId.patchValue([this.selectsSecond[3]['options'][0]['id']]);
			this.setExecuted();
		}
	}
	/**
	 * Add budget.
	 *
	 * @memberof BudgetComponent
	 */
	public addBudget(): void
	{
		const budgets = this.form.controls.budgets.value;
		
		if (this.editIndex !== null) {
			const budget = budgets[this.editIndex];
			budget[4].id =  this.form.controls.yearId.value[0];
			budget[5].id =  this.form.controls.monthId.value[0];
			budget[6].id =  this.form.controls.weekId.value[0];

			if (this.form.controls.typeId.value[0] === 1) {
				budget[7].id =  this.form.controls.programId.value[0];
			} else {
				budget[7].id =  this.form.controls.conceptId.value[0];
			}

			budget[8].value = this.form.controls.total.value;

			this.editIndex = null;
		} else {
			let formsControlsNames = this.getFormControlNames();
			const budget = [];

			for (const { type, name } of formsControlsNames) {
				budget.push({
					name,
					value: (type === 'select') ? this.getSelectName(name, this.form.controls[name].value) : this.getInputName(name, this.form.controls[name].value),
					id: (type === 'select') ? this.getSelectId(name, this.form.controls[name].value) : null,
					typeOfMask: (type === 'input' && name == 'total') ? 1 : null
				});
			}
	
			budgets.push(budget);
		}

		this.setprojectionAndPercentaje(budgets);
		this.form.controls.budgets.setValue(budgets);
	}
	/**
	 * Get form control names.
	 *
	 * @memberof BudgetComponent
	 */
	public getFormControlNames(): Array<any> {
		let formsControlsNames = [
			{
				type: 'select',
				name: 'companyId'
			},
			{
				type: 'select',
				name: 'officeId'
			},
			{
				type: 'select',
				name: 'projectId'
			},
			{
				type: 'select',
				name: 'typeId'
			},
			{
				type: 'select',
				name: 'yearId'
			},
			{
				type: 'select',
				name: 'monthId'
			},
			{
				type: 'select',
				name: 'weekId'
			}
		];

		if (this.form.controls.typeId.value[0] === 1) {
			formsControlsNames = [...formsControlsNames, ...[{
				type: 'select',
				name: 'programId'
			}]];
		} else {
			formsControlsNames = [...formsControlsNames, ...[{
				type: 'select',
				name: 'conceptId'
			}]];
		}

		formsControlsNames = [...formsControlsNames, ...[{
			type: 'input',
			name: 'total'
		}]];

		return formsControlsNames;
	}
	/**
	 * Get select name.
	 *
	 * @memberof BudgetComponent
	 */
	public getSelectName(name, value): void
	{
		var allSelects = this.selects.concat(this.selectsSecond);
		var select = allSelects.find(x => x["name"] === name);
		var option = select['options'].find(x => x['id'] === value[0]);
		return option.name ? option.name : option.unit;
	}
	 /**
	 * Get select name.
	 *
	 * @memberof BudgetComponent
	 */
	public getInputName(name, value): void
	{
		var result = value;

		//if (name !== 'paidDate') {
		//	result = formatter.format(value);
		//}

		return result;
	}
	/**
	 * Get select id.
	 *
	 * @memberof BudgetComponent
	 */
	public getSelectId(name, value): void
	{
		var allSelects = this.selects.concat(this.selectsSecond);
		var select = allSelects.find(x => x["name"] === name);
		var option = select['options'].find(x => x['id'] === value[0]);

		return option.name ? option.id : option.unit;
	}
	/**
	 * Edit table row select name.
	 *
	 * @memberof BudgetComponent
	 */
	public editTableRow(index): void
	{
		const budget = this.form.controls.budgets.value[index];
		this.form.controls.yearId.setValue([budget[4].id]);
		this.form.controls.monthId.setValue([budget[5].id]);
		this.form.controls.weekId.setValue([budget[6].id]);

		if (this.form.controls.typeId.value[0] === 1) {
			this.form.controls.programId.setValue([budget[7].id]);
		} else {
			this.form.controls.conceptId.setValue([budget[7].id]);
		}

		this.form.controls.total.setValue(budget[8].value);
		
		this.editIndex = index;
	}
	/**
	 * Set executed row.
	 *
	 * @memberof BudgetComponent
	 */
	public setExecuted(): void
	{
		const array = this.form.controls.typeId.value[0] === 1 ? this.form.controls.sales.value : this.form.controls.expenses.value;

		const customFiltered = array.filter(x =>
			x.companyId === this.form.controls.companyId.value[0] && x.officeId === this.form.controls.officeId.value[0] && x.projectId === this.form.controls.projectId.value[0]
		);
		
		if (customFiltered.length > 0) {
			if (this.form.controls.typeId.value[0] === 1) {
				this.form.controls.executed.patchValue(customFiltered.length);
			} else {
				var total = 0.00;

				for (const filter of customFiltered) {
					for (const expense of filter.expenses) {
						total += parseFloat(expense.totalPrice)
					}
				}
				this.form.controls.executed.patchValue(String(total));
			}
		}
	}
	/**
	 * Add budget in database.
	 *
	 * @memberof BudgetComponent
	 */
	public async saveBudget(): Promise<any> {
		const form = await this.getValues();
		this.myapp.setLoading(true);
		try {
			let response = null;
			if (form['id']) {
				response = await this.apiProvider.doPut('Budgets/UpdateBudget', form);
			} else {
				response = await this.apiProvider.doPost('Budgets/SaveBudget', form);
			}
			if (response['data'] === null) {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text:
						'No puedes cambiar de empresa porque esta sucursal tiene usuarios asociados, remueve esta asociación para completar el proceso.'
				});
			} else {
				this.doCancel();
			}
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Get values from array.
	 *
	 * @private
	 * @returns {object}
	 * @memberof BudgetComponent
	 */
	private getValues(): object {
		const form = {};

		if (this.budget) {
			form['id'] = this.budget.id;
		}

		form['budgetDetails'] = this.getTableValues();
		
		for (const select of selects) {
			form[select.name] = this.form.get(select.name).value[0];

			if (select.inputs) {
				for (const input of select.inputs) {
					form[input.name] = String(this.form.get(input.name).value);
				}
			}
		}

		return form;
	}
	/**
	 * Delete budget from database.
	 *
	 * @returns {Promise<any>}
	 * @memberof BudgetComponent
	 */
	public async deleteBudget(): Promise<any> {
		Swal.fire({
			title: '¿Estas seguro de eliminar este presupuesto?',
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#00b800',
			cancelButtonColor: '#313731ad',
			confirmButtonText: 'Si',
			cancelButtonText: 'No'
		}).then(async (result) => {
			if (result.value) {
				this.myapp.setLoading(true);
				try {
					await this.apiProvider.doDelete('Budgets/DeleteBudget', this.budget.id);
					this.doCancel();
					this.myapp.setLoading(false);
				} catch (err) {
					this.myapp.setLoading(false);
				}
			}
		});
	}
	/**
	 * Get values from table.
	 *
	 * @private
	 * @returns {Array<object>}
	 * @memberof BudgetComponent
	 */
	private getTableValues(): Array<object> {
		const values = [];

		for (const budget of this.form.controls.budgets.value) {
			const budgetRequest: any = {
			'yearId': budget[4].id,
			'monthId': budget[5].id,
			'weekId': budget[6].id,
			'programId': this.form.controls.typeId.value[0] === 1 ? budget[7].id : null,
			'conceptId': this.form.controls.typeId.value[0] === 0 ? budget[7].id : null,
			'total': String(budget[8].value)
			}

			values.push(budgetRequest);
		}

		return values;
	}
	/**
	 * Set executed row.
	 *
	 * @memberof BudgetComponent
	 */
	 public setprojectionAndPercentaje(budgets): void
	 {
		 let projection = 0.00;

		 for (const budget of budgets) {
			projection += parseFloat(budget[8].value);
		 }

		 const advancePercentage = (parseFloat(this.form.controls.executed.value) * 100) / projection;
		 this.form.controls.advance.patchValue(String(advancePercentage));
		 this.form.controls.projection.patchValue(String(projection));
	 }
	/**
	 * Delete table row.
	 *
	 * @memberof BudgetComponent
	 */
	public deleteTableRow(index): void
	{
		const budgets = this.form.controls.budgets.value;

		if (budgets.length > 0) {
			this.editIndex = null
			budgets.splice(index, 1);
			this.form.controls.budgets.setValue(budgets);
			this.setprojectionAndPercentaje(budgets);
		}
	}
	/**
	 * Get budgets.
	 *
	 * @memberof SaleComponent
	 */
	public getBudgets (): Array<object> {
		if (this.budget) {
		const {
			budgets
		} = this.budget;
		
		let budgetsResult = [];
		budgets.forEach(budget => {
			let formsControlsNames: any = [
			{
				name: 'companyId',
				'value': this.selects[0]['options'].find(x => x.id === this.budget.companyId).name,
				'id': this.budget.companyId
			},
			{
				name: 'officeId',
				'value': this.selects[1]['options'].find(x => x.id === this.budget.officeId).name,
				'id': this.budget.officeId
			},
			{
				name: 'projectId',
				'value': this.selects[2]['options'].find(x => x.id === this.budget.projectId).name,
				'id': this.budget.projectId
			},
			{
				name: 'typeId',
				'value': this.selects[3]['options'].find(x => x.id === this.budget.typeId).name,
				'id': this.budget.typeId
			},
			{
				name: 'yearId',
				'value': this.selectsSecond[0]['options'].find(x => x.id === budget.yearId).name,
				'id': budget.yearId
			},
			{
				name: 'monthId',
				'value': this.selectsSecond[1]['options'].find(x => x.id === budget.monthId).name,
				'id': budget.monthId
			},
			{
				name: 'weekId',
				'value': this.selectsSecond[2]['options'].find(x => x.id === budget.weekId).name,
				'id': budget.weekId
			}];

			if (this.budget.typeId === 1) {
				formsControlsNames = [
					...formsControlsNames,
					...[
						{
							name: 'programId',
							value: this.selectsSecond[3]['options'].find(x => x.id === budget.programId).name,
							id: budget.programId
						}
					]
				];
			} else {
				formsControlsNames = [
					...formsControlsNames,
					...[
						{
							name: 'conceptId',
							value: this.selectsSecond[3]['options'].find(x => x.id === budget.conceptId).name,
							id: budget.conceptId
						}
					]
				];
			}

			formsControlsNames = [
				...formsControlsNames,
				...[
					{
						name: 'total',
						value: budget.total,
						id: null,
						typeOfMask: 1
					}
				]
			];

			const expenseResult = [];
	
			for (const { name, value, id, typeOfMask } of formsControlsNames) {
				expenseResult.push({
					name,
					value,
					id,
					typeOfMask
				});
			}
			budgetsResult.push(expenseResult);
		});

		return budgetsResult;
		}
		
		return [];
	}
	/**
	 * Enter here when clicked cancel button.
	 *
	 * @memberof BudgetComponent
	 */
	public doCancel(): void {
		this.router.navigate([ { outlets: { app: 'budgets' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent
		});
	}
}
