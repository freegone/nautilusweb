/*Angular imports*/
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
/*Relatice Imports */
import { AppComponent } from '../../../../app/app.component';
import { ApiProvider } from '../../../../providers/providers.module';
import { input } from './template.json';
/**
 * The component of expenses.
 *
 * @export
 * @class ExpensesComponent
 */
@Component({
	selector: 'app-expenses',
	templateUrl: './expenses.component.html',
	styleUrls: [ './expenses.component.scss' ]
})
export class ExpensesComponent {
	/**
	 * Local reference to array of expenses.
	 *
	 * @type {Array<object>}
	 * @memberof ExpensesComponent
	 */
	public expenses: Array<object> = [];
	/**
	 * Local reference to array of templates.
	 * 
	 * @type {Array<object>}
	 * @memberof ExpensesComponent
	 */
	public originalExpenses: Array<object> = [];
	/**
	 * Save template object.
	 *
	 * @type {Array<object>}
	 * @memberof ExpensesComponent
	 */
	public searchInput: object = input;
	/**
	 * Inputs template.
	 *
	 * @type {Array<object>}
	 * @memberof ExpensesComponent
	 */
	public form: FormGroup;
	/**
	 *Creates an instance of ExpensesComponent.
	 * @param {Router} router - Includes router for navigation.
	 * @param {ActivatedRoute} activeRouter - Includes router for navigation.
	 * @param {ApiProvider} apiProvider - Includes api for call to database.
	 * @param {AppComponent} myapp - Includes app component for set loading.
	 * @memberof ExpensesComponent
	 */
	constructor(
		private router: Router,
		private activeRouter: ActivatedRoute,
		private apiProvider: ApiProvider,
		public myapp: AppComponent
	) {
		this.form = new FormGroup({
			search: new FormControl("")
		});
	}
	/**
	 * The function to execute each time when enter to module.
	 *
	 * @memberof ExpensesComponent
	 */
	public ngOnInit(): void {
		this.myapp.setLoading(true);
		this.expenses = [];
		this.loadTemplates();
	}
	/**
	 * The function to load templates from database of companies.
	 *
	 * @returns {Promise<any>}
	 * @memberof ExpensesComponent
	 */
	public async loadTemplates(): Promise<any> {
		try {
			const expenses = await this.apiProvider.doGet('Expenses/GetExpenses');
			this.expenses = expenses['data'];
			this.originalExpenses = expenses['data'];
			this.myapp.setLoading(false);
		} catch (err) {
			this.myapp.setLoading(false);
		}
	}
	/**
	 * Enter when input change.
	 *
	 * @memberof ExpensesComponent
	 */
	public search(text): void {
		if (text === "") {
			this.expenses = this.originalExpenses;
		} else if (this.originalExpenses.length > 0) {
			this.expenses = this.originalExpenses.filter(x => x["name"].toLowerCase().includes(text.toLowerCase()));
		}
	}
	/**
	 * Enter when clicked add button.
	 *
	 * @memberof ExpensesComponent
	 */
	public addExpense(expense = null): void {
		this.router.navigate([ { outlets: { app: 'expense' } } ], {
			skipLocationChange: true,
			relativeTo: this.activeRouter.parent,
			state: {
				data: expense
			}
		});
	}
}
