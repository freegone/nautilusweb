/*Angular imports*/
import { Component } from '@angular/core';
/**
 * The component of app.
 *
 * @export
 * @class AppComponent
 */
@Component({
	selector: 'app-app',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
	/**
  * Boolean value of show.
  *
  * @type {boolean}
  * @memberof AppComponent
  */
	public _show: boolean;
	/**
  * Object of submenu.
  *
  * @type {object}
  * @memberof AppComponent
  */
	public _submenu: object;
	/**
  * Enter here when clicked menu option.
  *
  * @param {*} event receive event.
  * @param {*} show receive show value.
  * @memberof AppComponent
  */
	public openSubMenu(event, show): void {
		this._show = show;
		if (this._show) {
			this._submenu = event;
		}
	}
}
