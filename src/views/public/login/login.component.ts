/*Angular imports*/
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
/*Relative imports*/
import Swal from 'sweetalert2';
import { ApiProvider } from '../../../providers/providers.module';
import { inputs, buttons } from './template.json';
import { AppComponent } from '../../../app/app.component';
/**
 * The component of login.
 *
 * @export
 * @class LoginComponent
 */
@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: [ './login.component.scss' ]
})
export class LoginComponent {
	/**
    * Inputs template.
    *
    * @type {Array<object>}
    * @memberof LoginComponent
    */
	public inputs: Array<object> = inputs;
	/**
    * Inputs template.
    *
    * @type {Array<object>}
    * @memberof LoginComponent
    */
	public form: FormGroup;
	/**
    * Buttons template.
    *
    * @type {object}
    * @memberof LoginComponent
    */
	public buttons: Array<object> = buttons;
	/**
    * Component constructor.
    * @param Router - Included router for navigation.
    */
	constructor(private router: Router, private apiProvider: ApiProvider, public myapp: AppComponent) {
		this.form = new FormGroup({
			email: new FormControl('', [
				Validators.required,
				Validators.email,
				Validators.minLength(5),
				Validators.maxLength(50)
			]),
			password: new FormControl('', [ Validators.required, Validators.minLength(1), Validators.maxLength(15) ])
		});
	}
	/**
    * Enter when clicked in log in button.
    *
    * @memberof LoginComponent
    */
	public async goLogin(): Promise<any> {
		this.myapp.setLoading(true);
		const user = await this.apiProvider.doPost('Login/DoLogin', this.form.value);
		this.myapp.setLoading(false);

		if (user['data'] === null) {
			Swal.fire({
				type: 'error',
				title: 'Oops...',
				text: 'Inicio de sesión incorrecto',
				confirmButtonColor: '#313731ad',
			});
			return;
		}

		localStorage.setItem('nautilusSession', JSON.stringify(user["data"]));
		this.router.navigate([ 'app' ]);
	}
}
