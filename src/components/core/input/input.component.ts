/* Angular Imports */
import { FormGroup } from '@angular/forms';
import { Component, Input, EventEmitter, Output } from '@angular/core';
/* Relative Imports */
import Swal from 'sweetalert2';
/**
 * The component of input.
 *
 * @export
 * @class InputComponent
 */
@Component({
	selector: 'app-input',
	templateUrl: './input.component.html',
	styleUrls: [ './input.component.scss' ]
})
export class InputComponent {
	/**
	 * Receive parent form.
	 *
	 * @type {FormGroup}
	 * @memberof InputComponent
	 */
	@Input('parentForm') parentForm: FormGroup;
	/**
	 * Receive input data.
	 *
	 * @type {object}
	 * @memberof InputComponent
	 */
	@Input('data') public input: object;
	/**
	 * Emitter an event.
	 *
	 * @private
	 * @memberof InputComponent
	 */
	@Output('tapEvent') private tap = new EventEmitter<any>();
	/**
	 * Enter here when upload file in input component.
	 *
	 * @param {*} e
	 * @memberof InputComponent
	 */
	public fileChange(e): void {
		if (e.target.type === 'file') {
			const _split = e.target.files[0].type.split('/');
			const _file = _split[0];
			const _type = _split[1];
			if (_file === 'image' && (_type === 'jpg' || _type === 'jpeg' || _type === 'png')) {
				this.tap.emit(this.tap.emit(e.target.files[0]));
			} else {
				Swal.fire({
					type: 'error',
					title: 'Lo sentimos',
					confirmButtonColor: '#313731ad',
					text: 'Solo se aceptan imagenes JPG, PNG.'
				});
				e.target.value = '';
				this.tap.emit('default');
			}
		} else if (e.target.type === 'checkbox') {
			this.tap.emit(e.currentTarget.checked);
		}
	}
	/**
	 * Enter here when upload file in input component.
	 *
	 * @param {*} e
	 * @memberof InputComponent
	 */
	public typeValue(value): void {
		this.tap.emit(value);
	}
}
