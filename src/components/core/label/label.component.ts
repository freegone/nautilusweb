import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit {
  /**
	 * Local reference @Input to any of type.
	 */
	@Input('data') public _data: object;
  constructor() { }

  ngOnInit() {
  }

}
