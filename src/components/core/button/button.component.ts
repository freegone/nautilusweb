/* Angular Imports */
import { Component, Input, Output, EventEmitter } from '@angular/core';
/**
 * The component of button.
 *
 * @export
 * @class ButtonComponent
 */
@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  /**
	 * Local reference @Input to any of type.
	 */
  @Input('data') public data: object;
  /**
	 * Local reference @Input to any of type.
	 */
  @Input('disabled') public disabled: boolean;
  /**
	 * Local reference @output to event emitter of tap.
	 */
	@Output('tapEvent') private tap = new EventEmitter<any>();
	/**
	 * Function to click in chat view and emit one instruction of chat.
	 *
	 * @memberof ButtonComponent
	 */
	public click(): void {
		this.tap.emit();
	}
}
