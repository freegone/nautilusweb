import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss']
})
export class SelectComponent implements OnInit {
	/**
	 * Local reference @Input to any of type.
	 */
	@Input('name') public name: string;
	/**
	 * Local reference @Input to any of type.
	 */
  	@Input('multiple') public _multiple: boolean = false;
	/**
	 * Local reference @Input to any of type.
	 */
  	@Input('options') public _options: object;
	/**
	 * Local reference @Input to any of type.
	 */
	@Input('disabled') public _disabled: object;
  	/**
	 * Local reference @output to event emitter of tap.
	 */
	@Output('tapEvent') private tap = new EventEmitter<any>();
	/**
	 * Receive parent form.
	 *
	 * @type {FormGroup}
	 * @memberof SelectComponent
	 */
	@Input('parentForm') parentForm: FormGroup;
	constructor() { }

	ngOnInit() {
	}
	changeOption(name, index){
		const option = this._options[index];
		option.formControlName = name;
		this.tap.emit(option);
	}
}
