import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {
	/**
	 * Receive columns data.
	 *
	 * @type {object}
	 * @memberof TableComponent
	 */
	@Input('columns') public columns: object;
	/**
	 * Receive rows data.
	 *
	 * @type {object}
	 * @memberof TableComponent
	 */
	@Input('rows') public rows: object;
	/**
	 * Receive selected row.
	 *
	 * @type {object}
	 * @memberof TableComponent
	 */
	@Input('selectedRow') public selectedRow: number;
	/**
	 * Emitter an event.
	 *
	 * @private
	 * @memberof TableComponent
	 */
	@Output('editEvent') private editEvent = new EventEmitter<any>();
	/**
	 * Emitter an event.
	 *
	 * @private
	 * @memberof TableComponent
	 */
	@Output('deleteEvent') private deleteEvent = new EventEmitter<any>();

	editRow(index) {
		this.editEvent.emit(index);
	}

	removeRow(index) {
		this.deleteEvent.emit(index);
	}

}
