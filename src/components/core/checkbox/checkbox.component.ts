/* Angular Imports */
import { FormGroup } from '@angular/forms';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss']
})
export class CheckboxComponent {
  /**
	 * Receive parent form.
	 *
	 * @type {FormGroup}
	 * @memberof InputComponent
	 */
	@Input('parentForm') parentForm: FormGroup;
	/**
	 * Receive input data.
	 *
	 * @type {object}
	 * @memberof InputComponent
	 */
	@Input('data') public checkbox: object;

	checkValue(event: any){
		console.log(event);
		console.log(this.parentForm);
		this.parentForm.controls['superUser'].setValue(!event)
	 }
}
