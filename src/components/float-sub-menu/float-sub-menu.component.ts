/* Angular Imports */
import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
/* Relative Imports */
import { AppComponent } from '../../app/app.component';
import Swal from 'sweetalert2';
import { template } from './resposibeTemplate.json';

@Component({
	selector: 'app-float-sub-menu',
	templateUrl: './float-sub-menu.component.html',
	styleUrls: [ './float-sub-menu.component.scss' ]
})
export class FloatSubMenuComponent implements OnChanges {
	_flag: boolean;
	_responsive = template;
	_referenceSubmenu: string;
  _timer: any;
  _auxIndex = 0;
	/**
	 * Local reference @Input to show of sub menu.
	 */
	@Input('show') public _show: boolean;
	/**
	 * Local reference @Input to show of sub menu.
	 */
	@Input('subMenu') public _subMenu: object;
	/**
	 * Local reference @output to event emitter of tap.
	 */
	@Output('hideEvent') private hideEvent = new EventEmitter<any>();
  /**
   *Creates an instance of FloatSubMenuComponent.
   * @param {Router} router
   * @param {ActivatedRoute} activeRouter
   * @param {AppComponent} myapp
   * @memberof FloatSubMenuComponent
   */
  constructor(private router: Router, private activeRouter: ActivatedRoute, public myapp: AppComponent) {
		this._referenceSubmenu = '';
	}
	ngOnChanges() {
		clearInterval(this._timer);
		this._flag = false;
	}
	hide() {
		if (this._flag) {
			this.hideEvent.emit();
		}
		this._flag = true;
		/*
    if(this._subMenu['key'] !== 'resposive') {
      /*this._timer = setTimeout(() => {
        this.hideEvent.emit();
      }, 2000);
     */
	}
	show() {
		clearInterval(this._timer);
	}
	showMenu(submenu) {
		this._referenceSubmenu = submenu;
	}
	openModule(menu) {
		this.hideEvent.emit();
		switch (menu.icon) {
			case 'sign-out':
				Swal.fire({
					title: '¿Estas seguro de cerrar sesión?',
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#00b800',
					cancelButtonColor: '#313731ad',
					confirmButtonText: 'Si',
					cancelButtonText: 'No'
				}).then((result) => {
					if (result.value) {
						localStorage.removeItem('nautilusSession');
						this.router.navigate([ 'login' ]);
					}
				});
				break;
			default:
				this.router.navigate([ { outlets: { app: [ menu.module ] } } ], {
					skipLocationChange: true,
					relativeTo: this.activeRouter
				});
				break;
		}
	}
}
