/** */
import { Component, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AppComponent } from '../../app/app.component';
import { template } from './template.json';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  /**
   * Define array of menu.
   */
  public _menu : object = template;
  
  /**
	 * Local reference @output to event emitter of tap.
	 */
	@Output('tapEvent') private tap = new EventEmitter<any>();
  /**
   * 
   */
  constructor(private router: Router,private activeRouter: ActivatedRoute, public myapp: AppComponent) {
  }
  /**
   * 
   * @param index 
   * @param option 
   * 
	 * @return { void }
   */
  openSubMenu(index, option): void {
    if(this._menu[index]['submenu']) {
      this.tap.emit({key:option,[option]: this._menu[index]['submenu']});
    } else {
      this.router.navigate([{ outlets: { app: [this._menu[index].module] }}],{skipLocationChange: true, relativeTo: this.activeRouter});
    }
  }
  /**
   * 
   * @param ids
   * 
	 * @return { boolean }
   */
  reviewPermission(ids): boolean {
    for (const id of ids) {
      const permission = this.myapp.reviewPermission(id,'actionView')
      if (permission) {
        return permission;
      }
    }

    return false;
  }

  refreshPage() {
    location.reload();
  }
}
