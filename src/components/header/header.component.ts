import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppComponent } from '../../app/app.component';
import { Router } from '@angular/router';
import { template } from "./template.json";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  /**
   * Sub menú.
   */
  public _subMenu : object = template;
  /**
	 * Local reference @output to event emitter of tap.
	 */
	@Output('tapEvent') private tap = new EventEmitter<any>();
  constructor(private router: Router, public myapp: AppComponent) { }

  ngOnInit() {
  }
  openSubMenu(option){
    this.tap.emit({ key:option, [option]: this._subMenu[option] });
  }
  refreshPage() {
    location.reload();
  }
  logOut() {
		Swal.fire({
      title: '¿Estas seguro de cerrar sesión?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00b800',
      cancelButtonColor: '#313731ad',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
        localStorage.removeItem('nautilusSession');
        this.router.navigate([ 'login' ]);
      }
    });
	}
}
