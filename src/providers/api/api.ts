/*Angular imports*/
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
/*Relative imports*/
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

/**
 * The injectable of api.
 */
@Injectable()
export class ApiProvider {
	/**
	 * Local reference to string of _baseUrl.
	 */
	private _baseUrl: string;
	/**
	 * The 'constructor' to declared imports and initialize variables for api.
	 * @param http load http for using output called to server.
	 */
	constructor(private http: HttpClient) {
		this._baseUrl = environment.api;
	}
	/**
	 * The function to get base url from the server.
	 */
	public getBaseUrl() {
		return this._baseUrl;
	}
	/**
	 * The function to call database and delete datas.
	 */
	public doDelete(url: string, id: any): Promise<any> {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json'
			}),
			params: {
				id
			}
		};
		return new Promise((resolve, reject) => {
			this.http.delete(this._baseUrl + url, options).map((res) => res).subscribe(
				(data) => {
					resolve(data);
				},
				(err) => {
					reject({ error: err });
				}
			);
		});
	}
	/**
	 * The function to download archives.
	 */
	public doDownload(url: string): Observable<any> {
		const headers = new HttpHeaders({
			Accept: 'application/pdf',
			'Content-Type': 'application/pdf'
		});
		return this.http
			.get(this._baseUrl + url, {
				headers: headers,
				responseType: 'blob'
			})
			.map((res) => res)
			.catch((err) => Observable.throw(err.error));
	}
	/**
	 * The function to call database and get datas.
	 */
	public doGet(url: string): Promise<any> {
		return new Promise((resolve, reject) => {
			this.http.get(this._baseUrl + url).map((res) => res).subscribe(
				(data) => {
					resolve(data);
				},
				(err) => {
					reject({ error: err });
				}
			);
		});
	}
	/**
	 * The function to call database and send datas.
	 */
	public doPost(url: string, data: any) {
		const session = JSON.parse(localStorage.getItem('nautilusSession'));
		data.creatorUser = session ? session.email : "";
		return new Promise((resolve, reject) => {
			this.http.post(this._baseUrl + url, data).map((res) => res).subscribe(
				(data) => {
					resolve(data);
				},
				(err) => {
					reject({ error: err });
				}
			);
		});
	}
	/**
	 * The function to call database and put datas.
	 */
	public doPut(url: string, data: any) {
		const session = JSON.parse(localStorage.getItem('nautilusSession'));
		data.modifUser = session ? session.email : "";
		return new Promise((resolve, reject) => {
			this.http.put(this._baseUrl + url, data).map((res) => res).subscribe(
				(data) => {
					resolve(data);
				},
				(err) => {
					reject({ error: err });
				}
			);
		});
	}
}
