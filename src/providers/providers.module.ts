import { NgModule } from '@angular/core';
/**
 * import Providers.
 */
import { ApiProvider } from './api/api';

/**
 * export Providers.
 */
export { ApiProvider } from './api/api';

@NgModule({
	declarations: [
	],
	imports: [
	],
	entryComponents: [
	],
	providers: [
		ApiProvider
	]
})
export class ProvidersModule { }
