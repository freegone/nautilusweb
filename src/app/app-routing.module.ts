/*Angular imports*/
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
/*Relative imports*/
import { LoginComponent } from '../views/public/login/login.component';
import { AppComponent } from '../views/core/app/app.component';
import { HomeComponent } from '../views/core/home/home.component';
import { CompaniesComponent } from '../views/core/administration/companies/companies.component';
import { CompanyComponent } from '../views/core/administration/company/company.component';
import { OfficesComponent } from '../views/core/administration/offices/offices.component';
import { OfficeComponent } from '../views/core/administration/office/office.component';
import { ProjectsComponent } from '../views/core/projects/projects/projects.component';
import { ProjectComponent } from '../views/core/projects/project/project.component';
import { ProgramsComponent } from '../views/core/projects/programs/programs.component';
import { ProgramComponent } from '../views/core/projects/program/program.component';
import { PaymentMethodsComponent } from '../views/core/projects/paymentmethods/paymentmethods.component';
import { PaymentMethodComponent } from '../views/core/projects/paymentmethod/paymentmethod.component';
import { DepartmentsComponent } from '../views/core/projects/departments/departments.component';
import { DepartmentComponent } from '../views/core/projects/department/department.component';
import { CostsComponent } from '../views/core/projects/costs/costs.component';
import { CostComponent } from '../views/core/projects/cost/cost.component';
import { ConceptsComponent } from '../views/core/projects/concepts/concepts.component';
import { ConceptComponent } from '../views/core/projects/concept/concept.component';
import { AgentsComponent } from '../views/core/projects/agents/agents.component';
import { AgentComponent } from '../views/core/projects/agent/agent.component';
import { CustomersComponent } from '../views/core/customers/customers/customers.component';
import { CustomerComponent } from '../views/core/customers/customer/customer.component';
import { VendorsComponent } from '../views/core/vendors/vendors/vendors.component';
import { VendorComponent } from '../views/core/vendors/vendor/vendor.component';
import { BudgetsComponent } from '../views/core/modules/budgets/budgets.component';
import { BudgetComponent } from '../views/core/modules/budget/budget.component';
import { ExpensesComponent } from '../views/core/modules/expenses/expenses.component';
import { ExpenseComponent } from '../views/core/modules/expense/expense.component';
import { SalesComponent } from '../views/core/modules/sales/sales.component';
import { SaleComponent } from '../views/core/modules/sale/sale.component';
import { ProfilesComponent } from '../views/core/settings/profiles/profiles.component';
import { ProfileComponent } from '../views/core/settings/profile/profile.component';
import { UsersComponent } from '../views/core/settings/users/users.component';
import { UserComponent } from '../views/core/settings/user/user.component';

/**
 * All routes includes here.
 */
const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'app', component: AppComponent,
    children: [
      { path: 'home', outlet: "app", component: HomeComponent },
      { path: 'companies', outlet: "app", component: CompaniesComponent },
      { path: 'company', outlet: "app", component: CompanyComponent },
      { path: 'offices', outlet: "app", component: OfficesComponent },
      { path: 'office', outlet: "app", component: OfficeComponent },
      { path: 'projects', outlet: "app", component: ProjectsComponent },
      { path: 'project', outlet: "app", component: ProjectComponent },
      { path: 'programs', outlet: "app", component: ProgramsComponent },
      { path: 'program', outlet: "app", component: ProgramComponent },
      { path: 'paymentMethods', outlet: "app", component: PaymentMethodsComponent },
      { path: 'paymentMethod', outlet: "app", component: PaymentMethodComponent },
      { path: 'departments', outlet: "app", component: DepartmentsComponent },
      { path: 'department', outlet: "app", component: DepartmentComponent },
      { path: 'costs', outlet: "app", component: CostsComponent },
      { path: 'cost', outlet: "app", component: CostComponent },
      { path: 'concepts', outlet: "app", component: ConceptsComponent },
      { path: 'concept', outlet: "app", component: ConceptComponent },
      { path: 'agents', outlet: "app", component: AgentsComponent },
      { path: 'agent', outlet: "app", component: AgentComponent },
      { path: 'customers', outlet: "app", component: CustomersComponent },
      { path: 'customer', outlet: "app", component: CustomerComponent },
      { path: 'vendors', outlet: "app", component: VendorsComponent },
      { path: 'vendor', outlet: "app", component: VendorComponent },
      { path: 'budgets', outlet: "app", component: BudgetsComponent },
      { path: 'budget', outlet: "app", component: BudgetComponent },
      { path: 'expenses', outlet: "app", component: ExpensesComponent },
      { path: 'expense', outlet: "app", component: ExpenseComponent },
      { path: 'sales', outlet: "app", component: SalesComponent },
      { path: 'sale', outlet: "app", component: SaleComponent },
      { path: 'profiles', outlet: "app", component: ProfilesComponent },
      { path: 'profile', outlet: "app", component: ProfileComponent },
      { path: 'users', outlet: "app", component: UsersComponent },
      { path: 'user', outlet: "app", component: UserComponent },
      { path: '', outlet: "app", component: HomeComponent }
    ]
  },
  { path: '**', redirectTo: '/login', pathMatch: 'full' }
];
/**
 * Ng Module for routing.
 */
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

/**
 * Export Class App routing Module.
 */
export class AppRoutingModule { }
