/*Angular imports*/
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClickOutsideModule } from 'ng-click-outside';
/*Libraries imports*/
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxLoadingModule } from 'ngx-loading';
import { NgxMaskModule } from 'ngx-mask';
import { ProvidersModule } from '../providers/providers.module';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { CurrencyMaskInputMode, NgxCurrencyModule } from "ngx-currency";
// import { TooltipModule } from 'ng2-tooltip-directive';
import { AppRoutingModule } from './app-routing.module';
/*Components imports*/
import { AppComponent } from './app.component';
import { InputComponent } from '../components/core/input/input.component';
import { CheckboxComponent } from '../components/core/checkbox/checkbox.component';
import { ButtonComponent } from '../components/core/button/button.component';
import { ImageComponent } from '../components/core/image/image.component';
import { TableComponent } from '../components/core/table/table.component';
import { LabelComponent } from '../components/core/label/label.component';
import { SidebarComponent } from '../components/sidebar/sidebar.component';
import { HeaderComponent } from '../components/header/header.component';
import { FloatSubMenuComponent } from '../components/float-sub-menu/float-sub-menu.component';
import { SelectComponent } from '../components/core/select/select.component';
/*Views imports*/
import { LoginComponent } from '../views/public/login/login.component';
import { HomeComponent } from '../views/core/home/home.component';
import { AppComponent as RootComponent } from '../views/core/app/app.component';
import { CompaniesComponent } from '../views/core/administration/companies/companies.component';
import { CompanyComponent } from '../views/core/administration/company/company.component';
import { OfficesComponent } from '../views/core/administration/offices/offices.component';
import { OfficeComponent } from '../views/core/administration/office/office.component';
import { ProjectsComponent } from '../views/core/projects/projects/projects.component';
import { ProjectComponent } from '../views/core/projects/project/project.component';
import { ProgramsComponent } from '../views/core/projects/programs/programs.component';
import { ProgramComponent } from '../views/core/projects/program/program.component';
import { PaymentMethodsComponent } from '../views/core/projects/paymentmethods/paymentmethods.component';
import { PaymentMethodComponent } from '../views/core/projects/paymentmethod/paymentmethod.component';
import { DepartmentsComponent } from '../views/core/projects/departments/departments.component';
import { DepartmentComponent } from '../views/core/projects/department/department.component';
import { CostsComponent } from '../views/core/projects/costs/costs.component';
import { CostComponent } from '../views/core/projects/cost/cost.component';
import { ConceptsComponent } from '../views/core/projects/concepts/concepts.component';
import { ConceptComponent } from '../views/core/projects/concept/concept.component';
import { AgentsComponent } from '../views/core/projects/agents/agents.component';
import { AgentComponent } from '../views/core/projects/agent/agent.component';
import { CustomersComponent } from '../views/core/customers/customers/customers.component';
import { CustomerComponent } from '../views/core/customers/customer/customer.component';
import { VendorsComponent } from '../views/core/vendors/vendors/vendors.component';
import { VendorComponent } from '../views/core/vendors/vendor/vendor.component';
import { BudgetsComponent } from '../views/core/modules/budgets/budgets.component';
import { BudgetComponent } from '../views/core/modules/budget/budget.component';
import { ExpensesComponent } from '../views/core/modules/expenses/expenses.component';
import { ExpenseComponent } from '../views/core/modules/expense/expense.component';
import { SalesComponent } from '../views/core/modules/sales/sales.component';
import { SaleComponent } from '../views/core/modules/sale/sale.component';
import { ProfilesComponent } from '../views/core/settings/profiles/profiles.component';
import { ProfileComponent } from '../views/core/settings/profile/profile.component';
import { UsersComponent } from '../views/core/settings/users/users.component';
import { UserComponent } from '../views/core/settings/user/user.component';
import { CurrencyPipe } from '../pipes/currency/currency.pipe';

export const customCurrencyMaskConfig = {
    align: "left",
    allowNegative: true,
    allowZero: true,
    decimal: "2",
    precision: 2,
    prefix: "",
    suffix: "",
    thousands: ",",
    nullable: true,
    min: null,
    max: null,
    inputMode: CurrencyMaskInputMode.FINANCIAL
};
/**
 * Ng Module for app module.
 */
@NgModule({
	declarations: [
		AppComponent,
		InputComponent,
		CheckboxComponent,
		ButtonComponent,
		ImageComponent,
		TableComponent,
		LabelComponent,
		SidebarComponent,
		HeaderComponent,
		FloatSubMenuComponent,
		SelectComponent,
		LoginComponent,
		HomeComponent,
		RootComponent,
		CompaniesComponent,
		CompanyComponent,
		OfficesComponent,
		OfficeComponent,
		ProjectsComponent,
		ProjectComponent,
		ProgramsComponent,
		ProgramComponent,
		PaymentMethodsComponent,
		PaymentMethodComponent,
		DepartmentsComponent,
		DepartmentComponent,
		CostsComponent,
		CostComponent,
		ConceptsComponent,
		ConceptComponent,
		AgentsComponent,
		AgentComponent,
		CustomersComponent,
		CustomerComponent,
		VendorsComponent,
		VendorComponent,
		BudgetsComponent,
		BudgetComponent,
		ExpensesComponent,
		ExpenseComponent,
		SalesComponent,
		SaleComponent,
		ProfilesComponent,
		ProfileComponent,
		UsersComponent,
		UserComponent,
		CurrencyPipe
	],
	imports: [
		BrowserModule,
		ClickOutsideModule,
		HttpClientModule,
		FormsModule,
		ReactiveFormsModule,
		NgxLoadingModule.forRoot({}),
		NgxMaskModule.forRoot({}),
		ProvidersModule,
		SweetAlert2Module,
		NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
		AppRoutingModule
	],
	providers: [],
	bootstrap: [ AppComponent ]
})
export /**
 * Export Class App Module.
 */
class AppModule {}
