/*Angular imports*/
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ChangeDetectorRef } from '@angular/core';
/*Relative imports*/
import _ from 'underscore';
/**
 * The component of app.
 */
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: [ './app.component.scss' ]
})
export class AppComponent {
	/**
	 * Local reference to boolean of loading.
	 *
	 * @type {boolean}
	 * @memberof AppComponent
	 */
	public loading: boolean = false;
	/**
	 * Define profile user.
	 *
	 * @type {Array<object>}
	 * @memberof AppComponent
	 */
	public profile: Array<object>;
	/**
	 *Creates an instance of AppComponent.
	 * @param {Router} router - Includes router for app.
	 * @param {ChangeDetectorRef} cdRef - Includes changes detector for detect changes.
	 * @memberof AppComponent
	 */
	constructor(private router: Router, private cdRef: ChangeDetectorRef) {
		this.reviewSession();
	}
	/**
	 *  Set loading value.
	 *
	 * @param {*} value
	 * @memberof AppComponent
	 */
	public setLoading(value): void {
		this.loading = value;
		this.cdRef.detectChanges();
	}
	/**
	 * Set profile user.
	 *
	 * @param {*} profile
	 * @memberof AppComponent
	 */
	public setProfile(profile): void {
		this.profile = profile;
		this.cdRef.detectChanges();
	}
	/**
	 *  Review if have a profile.
	 *
	 * @param {*} moduleId
	 * @param {*} permission
	 * @returns {boolean}
	 * @memberof AppComponent
	 */
	public reviewPermission(moduleId, permission): boolean {
		if (this.profile) {
			const access = _.findWhere(this.profile['access']['web']['permissions'], { moduleId: moduleId });
			return access[permission];
		}

		return false;
	}
	/**
	 * Review the session of the user.
	 * 
	 * @return { void }
   	 */
	private reviewSession(): void {
		if (localStorage.getItem('nautilusSession')) {
			this.router.navigate([ 'app' ]);
		} else {
			this.router.navigate([ 'login' ]);
		}
	}
}
